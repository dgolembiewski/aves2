# AVES

This MEAN app was generated with Angular 6.0.0.
runs on local port 3000 via Express server.js

## CURRENT STARTING PROCEDURE

To run the Node Mongo Server:
- first cd into the node server, and run `$ npm install`
- `$ source ./cfg/config_dev.sh`
- `$ node server.js`
- check it with: http://localhost:3000/api/members

To run the Java API:
- From aves root project
- do `cd java-api`
- run `./gradlew clean build` (Linux/Max OS X) or `gradlew clean build` (Windows)
- run `java -jar build/libs/avesjapi-1.0.0-0.jar`

- run `java -Dspring.config.location=./java-api/config/ -jar java-api/build/libs/avesjapi-1.0.0-0.jar` (externalized config files)
- log file location is given in the `logback.xml` file
- open the file `avesjapi.log` to view the log events
- upon successful start, API documentation can be viewed by going to:
    
    http://localhost:8080/swagger-ui.html

To run Angular front end:
- first, cd into the angular app, and run `$npm install`
- `$ npm start (or you can use $ ng serve)`
- check it with: http://localhost:4200/api/members

## To test from outside (simulate production environment)
- build "dev-ip" environment
  locally edit (don't add to git staging) the aves/angular-front/src/environments/environment.dev-ip.ts file --> put your own IP (e.g. VM IP) into it instead of (192.168.56.101)
  next, run this from angular folder
  `ng build --configuration=dev-ip`

- if you haven't done so yet, install http-server
  `sudo npm -g install http-server`

- run http-server from angular folder
  `http-server -c-1 -p 12345 ./dist`

- navigate to http://"your dev server IP":12345 in browser on host other than your dev server that can access it to test 

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Dockerfile(s)
- angular js component (angular-front folder)

  build with `docker build -t aves-angularjs .`

  run with:
  `docker run -it --rm -p 12345:80 -e ANGULAR_CONFIG="" --name my-aves-angularjs aves-angularjs`
  
  substitute "production" in ANGULAR_CONFIG for production environent, or "dev-ip" for production simulation

  test from browser: http://localhost:12345 (server side) or http://"your dev server IP":12345 (client side)

- node js component (node-express-mongo folder)
  build with `docker build -t aves-nodejs .`

  run with:

  `docker run -it 
  -e PORT=3000 
  -e MONGODB_URL="mongodb://avesuser:avesuser@ec2-54-210-232-74.compute-1.amazonaws.com:27017/avesdb"
  --rm -p 3000:3000 --name my-aves-nodejs aves-nodejs`

- java api component: java-api

  do `cd java-api`
  
  build with: `./gradlew buildImage`, 
  
  run command `docker images` 
  
  look for the image with name `aves/avesjapi`
  
  run with:
  `docker run -d -v PATH_TO_IDE_WORKSPACE/aves/java-api/config:/app/aves/avesjapi/mount -p 5000:8080 aves/avesjapi`
  
  API documentation can be viewed by going to:
      
      http://localhost:5000/swagger-ui.html
      
  run following `curl` command to verify:
  
  `curl -i -X GET --header "Content-Type: application/json" http://127.0.0.1:5000/aves/api/auth/checkUsernameAvailability?username=SOME_USERNAME`
  
  `{"available":true/false}`
  
  
## Dockerfile-Compose
To Build/Run Full-Stack
Run: 
`docker-compose up` This will build and run all containers
  - localhost:80
  - localhost:8080/userservice/users
  - localhost:3000/api/members

To Build/Rebuild services
Run: 
`docker-compose build` OR 
`docker-compose build [SERVICE]` (Replace [SERVICE] w/ angular-front, node-express-mongo, or java-api)

To Start Single Container
Run:
`docker-compose start [SERVICE]` (Replace [SERVICE] w/ angular-front, node-express-mongo, or java-api)

To Stop Single Container
Run:
`docker-compose stop [SERVICE]` (Replace [SERVICE] w/ angular-front, node-express-mongo, or java-api)

To Stop and Remove all Containers
Run: 
`docker-compose down`
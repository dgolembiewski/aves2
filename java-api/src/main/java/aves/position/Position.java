package aves.position;

import aves.common.model.Auditable;
import aves.user.User;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;


/**
 * The persistent class for the position database table.
 * 
 */
@Entity
@NamedQuery(name="Position.findAll", query="SELECT p FROM Position p")
public class Position extends Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="name")
	private String name;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="position")
	@JsonIgnore
	private List<User> users;

	public Position() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setPosition(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setPosition(null);

		return user;
	}

}
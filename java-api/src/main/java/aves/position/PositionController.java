package aves.position;

import aves.common.exception.AppException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/aves/api/positions")
@ResponseStatus(HttpStatus.OK)
public class PositionController {

    @Autowired
    PositionService positionService;

    @GetMapping
    public List<Position> getAllPosition() {
        return positionService.findAll();
    }

    @GetMapping("{id}")
    public Position getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return positionService.findById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody Position position) {

        if ( position.getId() != null ) {
            throw new AppException("Insert with id is not allowed.  Do you want to update?");
        }
        // TODO to return url to entity.
        positionService.insert(position);
        return "ok";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("{id}")
    public String update(@PathVariable() Long id,@RequestBody Position position) {
         // TODO to return url to entity.
        if ( position.getId() != null && position.getId() != id ) {
            throw new AppException("Path id does not equal Object Id!");
        }
        position.setId(id);
        positionService.update(position);
        return "ok";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        positionService.delete(id);
        return "ok";
    }

}
package aves.user;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserRepository  extends JpaRepository<User, UUID> {

	@Query("from User where isDeleted = false")
	List<User> findAll();

	@Query("from User where id = :id and isDeleted = false")
	Optional<User> findById(UUID id);

	@Query("from User where firstName = :firstName and isDeleted = false")
	Optional<User> findByFirstName(String firstName);

	@Query("from User where lastName = :lastName and isDeleted = false")
	Optional<User> findByLastName(String lastName);

	@Query("from User where email = :email and isDeleted = false")
	Optional<User> findByEmail(String email);

    List<User> findByIdIn(List<UUID> userIds);

    Boolean existsByEmail(String email);

    List<User> findByCompanyId(Long company_id);
    
    List<User> findByIsFreeAgentAndIsDeleted(Boolean freeAgentFlag, Boolean deletedFlag);
    

    List<User> findByFirstNameContainsOrLastNameContainsOrNickNameContainsAllIgnoreCaseAndIsDeleted(String pFirstName, String pLastName, String pNickName, boolean deletedFlag);


    //Retrieves users with either first name/last name/nick name matching supplied search term
	@Query("SELECT u FROM User u WHERE " +
            "(LOWER(u.firstName) LIKE LOWER(CONCAT('%',:searchTerm, '%')) OR " +
            "LOWER(u.lastName) LIKE LOWER(CONCAT('%',:searchTerm, '%')) OR " +
            "LOWER(u.nickName) LIKE LOWER(CONCAT('%',:searchTerm, '%'))) and  isDeleted = false")
    List<User> findByName(@Param("searchTerm") String searchTerm);


    List<User> findByTeamId(Long team_id);

}
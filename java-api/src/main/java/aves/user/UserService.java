package aves.user;

import aves.auth.repository.LoginRepository;
import aves.common.exception.AppException;
import aves.common.exception.AvesExceptionHandler;
import aves.common.exception.NotFoundException;

import aves.company.CompanyRepository;
import aves.team.ManagerTeam;
import aves.team.ManagerTeamRepository;
import aves.team.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import aves.auth.security.UserPrincipal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;
    
    @Autowired    
    ProjectMemberSearchRepository projectMemberSearchRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    TeamRepository teamRepository;


    @Autowired
    ManagerTeamRepository managerTeamRepository;

    @Autowired
    private HttpServletRequest request;

    public List<User> findAll() {

        List<User> users = userRepository.findAll();
        setUserDeletableEditable(users);
        return users;

    }

    public List<User> findByName(String pName) {

        //findByFirstNameContainsOrLastNameContainsOrNickNameContainsAllIgnoreCaseAndIsDeleted(pName, pName, pName, false);
        //List<User> users = userRepository.findByName(pName);   
    	
    	List<User> userList = userRepository.findByName(pName);
        setUserDeletableEditable(userList);
    	if(userList != null && userList.size() > 0) {
        	List<String> srcIdList = getSrcIds(userList);
        	List<ProjectMember> pmList = projectMemberSearchRepository.findProjectMemberPhotoBySrcIds(srcIdList);
        	for(User user : userList)
        	{
        		for(ProjectMember pm : pmList)
        		{
        			if(user.getId().toString().equals(pm.getSrc_id()))
        			{
        				user.setPhoto(pm.getPhoto());
        				break;
        			}
        		}
        	}
    	}
        return userList;
    }
    
    
    private List<String> getSrcIds(List<User> userList){
    	List<String> srcIdList = new ArrayList<String>();
    	for(User user : userList)
    		if(user.getId() != null) 
    			srcIdList.add(user.getId().toString());
    	return srcIdList;
    }


    public List<User> findByFreeAgent(boolean freeAgentFlag) {

        List<User> users = userRepository.findByIsFreeAgentAndIsDeleted(freeAgentFlag, false);
        //List<User> users = userRepository.findByName(pName);
        setUserDeletableEditable(users);
        return users;

    }
    
    
    public User findById(UUID id) {

        Optional<User> user = userRepository.findById(id);
        if ( user.isPresent()) {
            User u = user.get();
            setUserDeletableEditable(u);
            return u;
        }
        throw new NotFoundException("No User with id = " + id );

    }

    public User insert(User user) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if ( auth instanceof AnonymousAuthenticationToken) {
            // user not authenticated.
            throw new AppException("Unauthorized user");
        }

        userRepository.save(user);
        return user;
    }


    public User update(User user) {

        Optional<User> cUser = userRepository.findById(user.getId());
        if ( !cUser.isPresent() ) {
            throw new NotFoundException("Could not fine user with id "+ user.getId());
        }
        User eUser = cUser.get();
        setUserDeletableEditable(eUser);

        if ( !eUser.isEditable() ) {
            // user not authenticated.
            throw new AppException("Unauthorized user");
        }

        userRepository.save(user);
        return user;
    }

    private void setUserDeletableEditable( User user ) {

        List<User> userList = new ArrayList<User>();
        userList.add((User)user);
        setUserDeletableEditable(userList);

    }

    private void setUserDeletableEditable( List<User> userList ) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Function<String,Boolean> hasRole = (String role) -> {
            return auth.getAuthorities().stream().anyMatch(r -> r.getAuthority().equals(role));
        };

        if ( auth instanceof AnonymousAuthenticationToken) {
            // user not authenticated.
            return;
        }

        if ( hasRole.apply("ROLE_ADMIN") ) {
            for ( User user : userList) {
                user.setDeletable(true);
                user.setEditable(true);
            }
            return;
        }

        UUID loggedInUserId =((UserPrincipal)auth.getPrincipal()).getId();
        Set<Long> teamIds = null;
        if ( hasRole.apply("ROLE_MANAGER") ) {
            // user manager lets figure out which team they manage.
            List<ManagerTeam> managerTeams = managerTeamRepository.findByUserId(loggedInUserId);
            teamIds = managerTeams.stream().map(r -> r.getTeam().getId()).collect(Collectors.toSet());
        }

        for ( User user : userList) {
            if ( teamIds != null && user.getTeam()!= null && teamIds.contains(user.getTeam().getId())) {
                user.setDeletable(true);
                user.setEditable(true);
            } else if ( user.getId().equals(loggedInUserId)) {
                // allow all logged in user to updated there own card.
                user.setDeletable(true);
                user.setEditable(true);
            }
        }

    }


    public String deleteUser( UUID userId ) {


        Optional<User> cUser = userRepository.findById(userId);
        if ( !cUser.isPresent() ) {
            throw new NotFoundException("Could not fine user with id "+ userId);
        }
        User eUser = cUser.get();
        setUserDeletableEditable(eUser);

        if ( !eUser.isDeletable() ) {
            // user not authorized.
            throw new AppException("Unauthorized user");
        }

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        Optional<User> user = userRepository.findById(userId);
        if ( user.isPresent() ) {
            User u = user.get();
            u.setDeletedByUserId(((UserPrincipal)auth.getPrincipal()).getId());
            u.setIsDeleted(true);
            userRepository.save(u);
            return "ok";
        }
        throw new NotFoundException("No User with id = " + userId );
    }



}
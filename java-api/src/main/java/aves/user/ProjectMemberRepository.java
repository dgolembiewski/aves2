package aves.user;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectMemberRepository extends MongoRepository<ProjectMember, String> { 
	
	
	@Query("{src_id:'?0'}")
	ProjectMember findBySrc_id(String pSrcId);
	
	

}

package aves.user;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author Kishore Majeti
 *
 */

@JsonIgnoreProperties(ignoreUnknown = true)
@Document(collection = "project_members")
public class ProjectMember {
	
		@Id
		private  ObjectId _id;

		private String name;
		private String email;
		private String bio;
		private String src_id;
		private String photo;
		private Skill[] skills;
		
		
		
		public ProjectMember() {
			super();
		}

		
		public Skill[] getSkills() {
			return skills;
		}
		
		public void setSkills(Skill[] skills) {
			this.skills = skills;
		}
		public ObjectId get_id() {
			return _id;
		}
		public void set_id(ObjectId _id) {
			this._id = _id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getBio() {
			return bio;
		}
		public void setBio(String bio) {
			this.bio = bio;
		}
		public String getSrc_id() {
			return src_id;
		}
		public void setSrc_id(String src_id) {
			this.src_id = src_id;
		}
		public String getPhoto() {
			return photo;
		}
		public void setPhoto(String photo) {
			this.photo = photo;
		}
		
}


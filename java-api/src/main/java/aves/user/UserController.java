package aves.user;

import aves.common.exception.AppException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/aves/api/users")
@ResponseStatus(HttpStatus.OK)

public class UserController {

    @Autowired
    UserRepository userInfoRepository;

    @Autowired
    UserService userInfoService;

    @GetMapping()
    public List<User> getUsers(@RequestParam(value="name", defaultValue = "", required = false) String name,
    		@RequestParam(value="free-agent", defaultValue = "", required = false) String freeAgent
    		) {

    	List<User> userList = null;
    	name = (name == null) ? "" : name.trim();
    	if(!name.isEmpty()) {
    		userList = userInfoService.findByName(name);
    	}
    	else if(!freeAgent.isEmpty()) {
    		if(freeAgent.equalsIgnoreCase("true")) {
    			userList= userInfoService.findByFreeAgent(true);
    		}
    		else if(freeAgent.equalsIgnoreCase("false")) {
    			userList = userInfoService.findByFreeAgent(false);
    		}
    	}
    	else {
       		userList = userInfoService.findAll();
    	}

        return userList;
    }


    @GetMapping("{id}")
    public User getById(@PathVariable("id") String id) {
        return userInfoService.findById(UUID.fromString(id));
    }


    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("")
    public User insert(@RequestBody User userInfo) {

        if ( userInfo.getId() != null ) {
            throw new AppException("Insert with id is not allowed.  Do you want to update?");
        }
        userInfoService.insert(userInfo);
        return userInfo;
    }


    @PutMapping("{id}")

    public String update(@PathVariable() String id,@RequestBody User userInfo) {

         // TODO to return url to entity.


        if ( userInfo.getId() != null && !id.equals(userInfo.getId().toString())) {
            throw new AppException("Path id does not equal Object Id!");
        }
        userInfoService.update(userInfo);
        return "ok";
    }


    @DeleteMapping("{id}")
    public String delete(@PathVariable UUID id) {
        // TODO to return url to entity.
        userInfoService.deleteUser(id);
        return "ok";
    }


}

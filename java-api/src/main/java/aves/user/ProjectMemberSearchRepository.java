package aves.user;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Kishore Majeti
 *
 */


@Repository
public class ProjectMemberSearchRepository {

	@Autowired
    MongoTemplate mongoTemplate;

	public List<ProjectMember> findProjectMembersBySrcIds(List<String> srcidList) {
		
		Criteria criteria = new Criteria();
		Query query = new Query(criteria);
		query.addCriteria(Criteria.where("src_id").in(srcidList));
		List<ProjectMember> pmList = mongoTemplate.find(query, ProjectMember.class);
		return pmList;
	}
	
	public List<ProjectMember> findProjectMemberPhotoBySrcIds(List<String> srcidList) {
		
		Criteria criteria = new Criteria();
		Query query = new Query(criteria);
		query.fields().include("src_id");
		query.fields().include("photo");
		query.addCriteria(Criteria.where("src_id").in(srcidList));
		List<ProjectMember> pmList = mongoTemplate.find(query, ProjectMember.class);
		return pmList;
	}

}

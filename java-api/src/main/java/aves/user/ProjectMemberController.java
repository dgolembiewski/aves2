package aves.user;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
/**
 * 
 * @author Kishore Majeti
 *
 */


@RestController
@RequestMapping("/aves/api/pms")
public class ProjectMemberController {
	@Autowired
	private ProjectMemberRepository repository;
	@Autowired
	private ProjectMemberSearchRepository searchRepository;

	
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<ProjectMember> getAll() {

    	List<String> srcIdList = new ArrayList<String>();
    	srcIdList.add("1234");
    	srcIdList.add("2b726fc3-8d3f-41eb-bb11-2eeb384b36c9");
    	srcIdList.add("41196387-0ab2-4c29-90b4-a5111525826d");
    	srcIdList.add("7b2e2138-459d-469c-ad85-35d4a292244f");
    	List<ProjectMember> list = searchRepository.findProjectMembersBySrcIds(srcIdList);
    	//List<ProjectMember> list = repository.findAll();
    	return list;
    }
    
    
    @GetMapping("{srcid}")    
    @ResponseStatus(HttpStatus.OK)
    public ProjectMember getProjectMember(@PathVariable("srcid") String src_id) {
    	return repository.findBySrc_id(src_id);
    }

}

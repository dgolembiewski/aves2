package aves.user;

import aves.common.model.Auditable;
import aves.common.model.UserActivation;
import aves.common.model.UserRole;
import aves.company.Company;
import aves.location.Location;
import aves.position.Position;
import aves.team.ManagerTeam;
import aves.team.Team;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.NaturalId;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.UUID;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User extends Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id", columnDefinition = "BINARY(16)")
	private UUID id;
	
	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	@Temporal(TemporalType.DATE)
	@Column(name="available_date")
	private Date availableDate;

	@Column(name="deleted_by_user_id")
	private UUID deletedByUserId;

	@NaturalId
    @NotBlank
    @Size(max = 40)
    @Email
    @Column(name="email")
	private String email;

	@Column(name="first_name")
	private String firstName;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="is_free_agent")
	private Boolean isFreeAgent;

	@Column(name="last_name")
	private String lastName;

	@Column(name="nick_name")
	private String nickName;

	@Column(name="phone")
	private String phone;

	@Transient
	private boolean isDeletable=false;
	
	@Transient
	private String photo;
	

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public boolean isDeletable() {
		return isDeletable;
	}

	public void setDeletable(boolean deletable) {
		isDeletable = deletable;
	}

	public boolean isEditable() {
		return isEditable;
	}

	public void setEditable(boolean editable) {
		isEditable = editable;
	}

	@Transient
	private boolean isEditable=false;

	//bi-directional many-to-one association to ManagerTeam
	@OneToMany(mappedBy="user")
	@JsonIgnore
	private List<ManagerTeam> managerTeams;

	//bi-directional many-to-one association to Company
	@ManyToOne
	@JoinColumn(name="company_id")
	private Company company;

	//bi-directional many-to-one association to Location
	@ManyToOne
	@JoinColumn(name="location_id")
	private Location location;

	//bi-directional many-to-one association to Position
	@ManyToOne
	@JoinColumn(name="position_id")
	private Position position;

	//bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name="subteam_id")
	private Team team;

	//bi-directional many-to-one association to UserActivation
	@OneToMany(mappedBy="user")
	@JsonIgnore
	private List<UserActivation> userActivations;

	//bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy="user")
	@JsonIgnore
	private List<UserRole> userRoles;

	public User() {
	}
	
	public User(String firstName, String lastName, String email)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public Date getAvailableDate() {
		return this.availableDate;
	}

	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}

	public UUID getDeletedByUserId() {
		return this.deletedByUserId;
	}

	public void setDeletedByUserId(UUID deletedByUserId) {
		this.deletedByUserId = deletedByUserId;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Boolean getIsFreeAgent() {
		return this.isFreeAgent;
	}

	public void setIsFreeAgent(Boolean isFreeAgent) {
		this.isFreeAgent = isFreeAgent;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public List<ManagerTeam> getManagerTeams() {
		return this.managerTeams;
	}

	public void setManagerTeams(List<ManagerTeam> managerTeams) {
		this.managerTeams = managerTeams;
	}

	public ManagerTeam addManagerTeam(ManagerTeam managerTeam) {
		getManagerTeams().add(managerTeam);
		managerTeam.setUser(this);

		return managerTeam;
	}

	public ManagerTeam removeManagerTeam(ManagerTeam managerTeam) {
		getManagerTeams().remove(managerTeam);
		managerTeam.setUser(null);

		return managerTeam;
	}

	public Company getCompany() {
		return this.company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}
	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public Position getPosition() {
		return this.position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public List<UserActivation> getUserActivations() {
		return this.userActivations;
	}

	public void setUserActivations(List<UserActivation> userActivations) {
		this.userActivations = userActivations;
	}

	public UserActivation addUserActivation(UserActivation userActivation) {
		getUserActivations().add(userActivation);
		userActivation.setUser(this);

		return userActivation;
	}

	public UserActivation removeUserActivation(UserActivation userActivation) {
		getUserActivations().remove(userActivation);
		userActivation.setUser(null);

		return userActivation;
	}

	public List<UserRole> getUserRoles() {
		return this.userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public UserRole addUserRole(UserRole userRole) {
		getUserRoles().add(userRole);
		userRole.setUser(this);

		return userRole;
	}

	public UserRole removeUserRole(UserRole userRole) {
		getUserRoles().remove(userRole);
		userRole.setUser(null);

		return userRole;
	}

}
package aves.role;

import aves.common.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
@RequestMapping("/aves/api/roles")
@ResponseStatus(HttpStatus.OK)
public class RoleController {

    @Autowired
    RoleService roleService;

    @GetMapping
    public List<Role> getAllPosition() {
        return roleService.findAll();
    }

    @GetMapping("{id}")
    public Role getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return roleService.findById(id);
    }

}

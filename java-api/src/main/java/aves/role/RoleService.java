package aves.role;

import aves.common.exception.NotFoundException;
import aves.common.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    public List<Role> findAll() {
        return roleRepository.findAll();
    }

    public Role findById(Long id ) {
        return roleRepository
                .findById(id)
                .orElseThrow(()->new NotFoundException("No Role with id "+id ));
    }
}

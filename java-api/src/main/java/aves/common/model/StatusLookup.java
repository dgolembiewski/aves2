package aves.common.model;

import javax.persistence.*;


/**
 * The persistent class for the status_lookup database table.
 * 
 */
@Entity
@Table(name="status_lookup")
@NamedQuery(name="StatusLookup.findAll", query="SELECT s FROM StatusLookup s")
public class StatusLookup extends Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer id;

	private String description;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	private String status;

	public StatusLookup() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
package aves.common.model;

import aves.user.User;

import javax.persistence.*;

/**
 * The persistent class for the user_activation database table.
 * 
 */
@Entity
@Table(name="user_activation")
@NamedQuery(name="UserActivation.findAll", query="SELECT u FROM UserActivation u")
public class UserActivation extends Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name="verification_hash")
	private String verificationHash;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	private User user;

	public UserActivation() {
	}
	
	public UserActivation(String verificationHash, User user)
	{
		this.verificationHash = verificationHash;
		this.user = user;
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVerificationHash() {
		return this.verificationHash;
	}

	public void setVerificationHash(String verificationHash) {
		this.verificationHash = verificationHash;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
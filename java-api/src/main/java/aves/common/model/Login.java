package aves.common.model;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.sql.Timestamp;
import java.util.UUID;


/**
 * The persistent class for the login database table.
 * 
 */
@Entity
@NamedQuery(name="Login.findAll", query="SELECT l FROM Login l")
public class Login extends Auditable {

	private static final long serialVersionUID = 6259295036252033372L;

	@Column(name="deleted_by_user_id")
	private UUID deletedByUserId;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	@Column(name="last_login_ts")
	private Timestamp lastLoginTs;
	
	@NotBlank
    @Size(max = 100)
	@Column(name="password")
	private String password;

	@NotBlank
    @Size(max = 15)
	@Column(name="username")
	private String username;

	//bi-directional many-to-one association to StatusLookup
	@ManyToOne
	@JoinColumn(name="status_id")
	private StatusLookup statusLookup;

	//bi-directional many-to-one association to User
	/*@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	private User user;*/

	@Id
	@Column(name="user_id")
	private UUID userId;

	public Login() {
	}
	
	public Login(String username, String password) 
	{
		this.username = username;
		this.password = password;
	}

	public UUID getDeletedByUserId() {
		return this.deletedByUserId;
	}

	public void setDeletedByUserId(UUID deletedByUserId) {
		this.deletedByUserId = deletedByUserId;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Timestamp getLastLoginTs() {
		return this.lastLoginTs;
	}

	public void setLastLoginTs(Timestamp lastLoginTs) {
		this.lastLoginTs = lastLoginTs;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public StatusLookup getStatusLookup() {
		return this.statusLookup;
	}

	public void setStatusLookup(StatusLookup statusLookup) {
		this.statusLookup = statusLookup;
	}

	/*public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}
*/

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}
}
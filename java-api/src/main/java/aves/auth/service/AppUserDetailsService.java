package aves.auth.service;

import aves.auth.repository.LoginRepository;
import aves.auth.security.UserPrincipal;
import aves.common.model.Login;
import aves.user.User;
import aves.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class AppUserDetailsService implements UserDetailsService {

    @Autowired
    private LoginRepository loginRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmail)
            throws UsernameNotFoundException {

    	Login login = new Login();
    	User user = new User();
    	
    	if(usernameOrEmail.contains("@"))
    	{
    		user = userRepository.findByEmail(usernameOrEmail).get();
    		Login active = loginRepository.findById(user.getId()).get();
            return UserPrincipal.create(userRepository, active);
    	}
    	else
    	{
            login = loginRepository.findByUsername(usernameOrEmail)
                    .orElseThrow(() ->
                            new UsernameNotFoundException("User not found with username or email: " + usernameOrEmail)
                    );
            return UserPrincipal.create(userRepository, login);
    	}

    }

    // This method is used by JWTAuthenticationFilter
    @Transactional
    public UserDetails loadUserById(UUID id) {
        Login login = loginRepository.findById(id).orElseThrow(
                () -> new UsernameNotFoundException("User not found with id : " + id)
        );

        return UserPrincipal.create(userRepository, login);
    }
}

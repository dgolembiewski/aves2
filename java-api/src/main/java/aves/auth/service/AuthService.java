package aves.auth.service;

import aves.auth.model.RoleName;
import aves.auth.model.StatusName;
import aves.auth.payload.*;
import aves.auth.repository.ActivationRepository;
import aves.auth.repository.LoginRepository;
import aves.role.RoleRepository;
import aves.auth.repository.StatusLookupRepository;
import aves.auth.security.JwtTokenProvider;
import aves.auth.security.UserPrincipal;
import aves.common.exception.AppException;
import aves.common.model.*;
import aves.common.security.Sha256Hash;
import aves.email.service.EmailService;
import aves.email.vo.ActivateUserEmailRequest;
import aves.email.vo.ResetPasswordEmailRequest;
import aves.user.User;
import aves.user.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class AuthService {

    private static final Logger logger = LoggerFactory.getLogger(AuthService.class);

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    UserRepository userRepository;

    @Autowired
    LoginRepository loginRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    StatusLookupRepository statusRepository;

    @Autowired
    ActivationRepository activationRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    EmailService emailService;

    public boolean checkUsernameAvailability(String username) throws AppException {
        return !loginRepository.existsByUsername(username);
    }

    public boolean checkEmailAvailability(String email) throws AppException {
        return !userRepository.existsByEmail(email);
    }

    @Transactional
    public Login registerUser(SignUpRequest signUpRequest) throws AppException {

        Optional<User> existingByEmail = userRepository.findByEmail(signUpRequest.getEmail());
        Optional<Login> existingByUsername = loginRepository.findByUsername(signUpRequest.getUsername());

        if(existingByUsername.isPresent() && existingByUsername.get().getUsername().equalsIgnoreCase(signUpRequest.getUsername()))
            throw new AppException("Username is already taken!");

        if(existingByEmail.isPresent() && existingByEmail.get().getEmail().equalsIgnoreCase(signUpRequest.getEmail()))
            throw new AppException("Email Address already in use!");

        logger.info("Creating the user");
        User user = new User(signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getEmail());
        user.setIsFreeAgent(Boolean.TRUE);
        user.setIsDeleted(Boolean.FALSE);

        Role role = roleRepository.findByName(RoleName.ROLE_USER.name())
                .orElseThrow(() -> new AppException("User Role not set."));

        UserRole userRole = new UserRole(user, role);
        ArrayList<UserRole> roleList = new ArrayList<>();
        roleList.add(userRole);

        user.setUserRoles(roleList);

        userRepository.save(user);

        // Login creation
        Login login = new Login(signUpRequest.getUsername(), passwordEncoder.encode(signUpRequest.getPassword()));
        login.setUserId(user.getId());
        login.setIsDeleted(Boolean.FALSE);

        StatusLookup status = statusRepository.findByStatus(StatusName.PENDING.name())
                .orElseThrow(() -> new AppException("Status not set."));
        login.setStatusLookup(status);

        loginRepository.save(login);

        // Activation creation
        UserActivation activation = new UserActivation((new Sha256Hash()).getSHA256Hash(login.getUsername().concat(user.getEmail())), user);
        
        activationRepository.save(activation);

        ActivateUserEmailRequest activateUserEmailRequest = new ActivateUserEmailRequest();
        activateUserEmailRequest.setToAddress(user.getEmail());
        activateUserEmailRequest.setUserName(login.getUsername());
        activateUserEmailRequest.setUuid(activation.getVerificationHash());

        emailService.sendActivateUserEmail(activateUserEmailRequest);

        return login;
    }

    public String login(LoginRequest loginRequest) throws AppException {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsernameOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        return tokenProvider.generateToken(authentication);
    }

    public UserSummary getCurrentUser(Authentication authentication) throws AppException {

        UserPrincipal currentUser = (UserPrincipal)authentication.getPrincipal();
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getUsername(), currentUser.getFirstName(), currentUser.getLastName());
        return userSummary;
    }

    @Transactional
    public boolean resetPassword(ResetRequest resetRequest)
    {
    	String hashToken;
    	
    	User user = null;
    	Login login = null;
    	
    	if(resetRequest.getUsernameOrEmail().contains("@"))
    	{
    		if(!userRepository.existsByEmail(resetRequest.getUsernameOrEmail()))
    		{
    			throw new AppException("Email address not found!");
    		}
    		else
    		{	
    			user = userRepository.findByEmail(resetRequest.getUsernameOrEmail()).get();
    			login = loginRepository.findById(user.getId()).get();
    		}
    	}
    	else
    	{
    		if(!loginRepository.existsByUsername(resetRequest.getUsernameOrEmail()))
    		{
    			throw new AppException("Username not found!");
    		}
    		else
    		{
        		login = loginRepository.findByUsername(resetRequest.getUsernameOrEmail()).get();
        		user = userRepository.findById(login.getUserId()).get();
    		}
    	}
    	
    	hashToken = new Sha256Hash().getSHA256Hash(login.getUsername().concat(String.valueOf(Instant.now().getEpochSecond())));
    	
    	UserActivation activation = new UserActivation();
    	activation.setUser(user);
    	activation.setVerificationHash(hashToken);
    	activationRepository.save(activation);
    	
    	ResetPasswordEmailRequest request = new ResetPasswordEmailRequest();
    	request.setToAddress(user.getEmail());
    	request.setId(user.getId());
    	request.setToken(hashToken);
    	
    	emailService.sendResetPasswordEmail(request);
    	
    	return true;
    }
    
    public boolean validateResetToken(String resetToken) throws ParseException
    {
    	Instant now = Instant.now();
		Instant timestamp = null;
		
		SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd hh:mm:ss a");
		DateTimeFormatter fm = DateTimeFormatter.ofPattern("uu/MM/dd hh:mm:ss a")
				.withLocale(Locale.US)
				.withZone(ZoneId.systemDefault());
    	
    	UserActivation act = activationRepository.findByVerificationHash(resetToken).get();
    	
    	timestamp = act.getCreatedAt();
    	
    	Date formattedStamp = format.parse(fm.format(timestamp));
    	Date formattedNow = format.parse(fm.format(now));
    	Calendar c = Calendar.getInstance();
    	c.setTime(formattedStamp);
    	c.add(Calendar.DATE, 1);
    	Date expires = c.getTime();
    	
    	if(formattedNow.before(expires))
    	{
    		return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    
    public Login updateUserPassword(PasswordUpdateRequest passwordUpdateRequest)
    {
    	User user = activationRepository.findByVerificationHash(passwordUpdateRequest.getToken()).get().getUser();
    	Login login = loginRepository.findById(user.getId()).get();
    	
    	login.setPassword(passwordEncoder.encode(passwordUpdateRequest.getNewPassword()));
    	
    	return loginRepository.save(login);
    }
}

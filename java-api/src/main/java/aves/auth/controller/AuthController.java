package aves.auth.controller;

import aves.auth.payload.*;
import aves.auth.service.AuthService;
import aves.common.exception.AppException;
import aves.common.model.Login;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.text.ParseException;

@RestController
@RequestMapping("/aves/api/auth")
public class AuthController {

    private static final Logger logger = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    AuthService authService;


    @GetMapping("/checkUsernameAvailability")
    public ResponseEntity<?> checkUsernameAvailability(@RequestParam(value = "username") String username) {
        try {
            Boolean isAvailable = authService.checkUsernameAvailability(username);
            return ResponseEntity.ok(new UserIdentityAvailability(isAvailable));
        }
        catch (AppException appException) {
            return new ResponseEntity<Object>(new ApiResponse(false, appException.getMessage()),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/checkEmailAvailability")
    public ResponseEntity<?> checkEmailAvailability(@RequestParam(value = "email") String email) {
        try {
            Boolean isAvailable = authService.checkEmailAvailability(email);
            return ResponseEntity.ok(new UserIdentityAvailability(isAvailable));
        }
        catch (AppException appException) {
            return new ResponseEntity<Object>(new ApiResponse(false, appException.getMessage()),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {

        try {
            Login result = authService.registerUser(signUpRequest);
            
            URI location = ServletUriComponentsBuilder
                    .fromCurrentContextPath().path("/users/{username}")
                    .buildAndExpand(result.getUsername()).toUri();

            return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
        }
        catch (AppException appException) {
            return new ResponseEntity<Object>(new ApiResponse(false, appException.getMessage()),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        try {
            String jwt = authService.login(loginRequest);
            return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
        }
        catch (AppException appException) {
            return new ResponseEntity<Object>(new ApiResponse(false, appException.getMessage()),
                    HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/summary")
    public ResponseEntity<?> getUserSummary(Authentication authentication) {

        try {
            return ResponseEntity.ok(authService.getCurrentUser(authentication));
        }
        catch (AppException appException) {
            return new ResponseEntity<Object>(new ApiResponse(false, appException.getMessage()),
                    HttpStatus.BAD_REQUEST);
        }
    }
    
    @PostMapping("/reset")
    public ResponseEntity<?> resetPasswordRequest(@Valid @RequestBody ResetRequest resetRequest)
    {
    	boolean success = false;
    	
		success = authService.resetPassword(resetRequest);
		
		if(success == true)
    	{
        	return ResponseEntity.ok(new ApiResponse(true, "Password reset email has been sent!"));
    	}
    	else
    	{
    		return new ResponseEntity<Object>(new ApiResponse(false, "Password reset email not sent!"),
    				HttpStatus.BAD_REQUEST);
    	}
    }
    
    @PostMapping("/updatePassword")
    public ResponseEntity<?> updatePassword(@Valid @RequestBody PasswordUpdateRequest passwordUpdateRequest)
    {
    	try 
    	{
        	boolean validated = authService.validateResetToken(passwordUpdateRequest.getToken());
        	
        	if(validated == true)
        	{
        		authService.updateUserPassword(passwordUpdateRequest);
        		return ResponseEntity.ok(new ApiResponse(true, "Password successfully updated!"));
        	}
        	else
        	{
        		return new ResponseEntity<Object>(new ApiResponse(false, "Token invalid or already expired!"),
        				HttpStatus.BAD_REQUEST);
        	}
    	}
    	catch(AppException appException)
    	{
    		return new ResponseEntity<Object>(new ApiResponse(false, "Password update unsuccessful!"),
    				HttpStatus.BAD_REQUEST);
    	} 
    	catch (ParseException e) 
    	{
    		return new ResponseEntity<Object>(new ApiResponse(false, "Password update unsuccessful!"),
    				HttpStatus.BAD_REQUEST);
		}
    }
}

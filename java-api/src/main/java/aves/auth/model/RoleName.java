package aves.auth.model;

public enum  RoleName {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_MANAGER
}
package aves.auth.model;

public enum StatusName {

    ACTIVE,
    INACTIVE,
    PENDING

}

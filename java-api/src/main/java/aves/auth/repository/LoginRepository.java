package aves.auth.repository;


import aves.common.model.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;


@Repository
public interface LoginRepository extends JpaRepository<Login, UUID>
{
	Optional<Login> findByUsername(String username);
	
	Boolean  existsByUsername(String username);
}

package aves.auth.repository;

import aves.common.model.UserActivation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ActivationRepository extends JpaRepository<UserActivation, Long> {

    Optional<UserActivation> findByVerificationHash(String verificationHash);

    Optional<UserActivation> findByUser(Long id);
}

package aves.auth.repository;


import aves.common.model.StatusLookup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StatusLookupRepository extends JpaRepository<StatusLookup, Long> {

    Optional<StatusLookup> findByStatus(String name);
}

package aves.auth.payload;

import javax.validation.constraints.NotBlank;

public class ResetRequest {

	@NotBlank
	private String usernameOrEmail;

	public String getUsernameOrEmail() {
		return usernameOrEmail;
	}

	public void setUsernameOrEmail(String usernameOrEmail) {
		this.usernameOrEmail = usernameOrEmail;
	}

}

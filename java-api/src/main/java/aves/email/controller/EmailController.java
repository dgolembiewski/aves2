package aves.email.controller;

import aves.email.service.EmailService;
import aves.email.vo.ActivateUserEmailRequest;
import aves.email.vo.EmailRequest;
import aves.email.vo.ResetPasswordEmailRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */


@RestController
@RequestMapping("/aves/api/email")
public class EmailController{

    @Autowired
    private EmailService emailService;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping("/hello1")
    public String getHello()
    {
        return "Hello World";
    }
  
    
    @PostMapping("/v1")
    public ResponseEntity<Object> sendEmail(@Valid @RequestBody EmailRequest pEmailReq)
    {
        logger.info("EmailController:sendEmail:pEmailReq = " + pEmailReq);
        emailService.sendEmail(pEmailReq);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    
    
    @PostMapping("/aves-activate-user/v1")
    public ResponseEntity<Object> sendActivateUserEmail(@Valid @RequestBody ActivateUserEmailRequest pEmailReq)
    {
        logger.info("EmailController:sendActivateUserEmail:pEmailReq =" + pEmailReq);
        emailService.sendActivateUserEmail(pEmailReq);        	
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
        

    @PostMapping("/aves-reset-passsword/v1")
    public ResponseEntity<Object> sendResetPasswordEmail(@Valid @RequestBody ResetPasswordEmailRequest pEmailReq)
    {
        logger.info("EmailController:sendActivateUserEmail:pEmailReq = " + pEmailReq);
        emailService.sendResetPasswordEmail(pEmailReq);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

}
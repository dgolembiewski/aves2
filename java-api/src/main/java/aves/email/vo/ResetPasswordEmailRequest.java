package aves.email.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

@ApiModel(description = "Reset Password - Email Request Payload")
public class ResetPasswordEmailRequest{

	@ApiModelProperty(notes="destination email address - Mandatory field")	
	@NotBlank
	@Email(message = "toAddress shouild be a valid email")
    private String toAddress;

    
	@ApiModelProperty(notes="Token -  Mandatory field")		
	@NotBlank(message = "token cannot be blank")	
    private String token;
	
	private UUID id;

    public String getToAddress()
    {
        return toAddress;
    }

    public void setToAddress(String pStr)
    {
        this.toAddress = pStr;
    }
    
    public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String toString() {
    	String s = String.join(System.getProperty("line.separator"),
    			"toAddress : " + toAddress);
    	return s;
    }
 
}
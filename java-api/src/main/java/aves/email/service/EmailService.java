
package aves.email.service;

import aves.common.exception.EmailServiceException;
import aves.email.vo.ActivateUserEmailRequest;
import aves.email.vo.EmailRequest;
import aves.email.vo.ResetPasswordEmailRequest;


/**
 * 
 * @author Kishore Majeti - 07/03/2018
 *
 */

public interface EmailService {
    
    public void sendEmail(EmailRequest pEmailReq) throws EmailServiceException;
    public void sendActivateUserEmail(ActivateUserEmailRequest pEmailReq) throws EmailServiceException;
    public void sendResetPasswordEmail(ResetPasswordEmailRequest pEmailReq) throws EmailServiceException;
    
}

package aves.company;

import aves.common.model.Auditable;
import aves.user.User;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the company database table.
 * 
 */
@Entity
@NamedQuery(name="Company.findAll", query="SELECT c FROM Company c")
public class Company extends Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	private String name;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="company")
	@JsonIgnore
	private List<User> users;

	public Company() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUsers(User users) {
		getUsers().add(users);
		users.setCompany(this);

		return users;
	}

	public User removeUsers(User users) {
		getUsers().remove(users);
		users.setCompany(null);

		return users;
	}
}
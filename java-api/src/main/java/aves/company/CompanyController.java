package aves.company;

import aves.common.exception.AppException;
import aves.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/aves/api/companies")
@ResponseStatus(HttpStatus.OK)
public class CompanyController {

    @Autowired
    CompanyService companyService;

    @GetMapping
    public List<Company> getAllCompany() {
        return companyService.findAll();
    }

    @GetMapping("{id}")
    public Company getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return companyService.findById(id);
    }

    @GetMapping("{id}/users")
    public List<User> getByIdUsers(HttpServletResponse response, @PathVariable("id") Long id) {
        return companyService.getAssociatedUsers(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody Company company) {

        if ( company.getId() != null ) {
            throw new AppException("Insert with id is not allowed.  Do you want to update?");
        }
        // TODO to return url to entity.
        companyService.insert(company);
        return "ok";
    }



    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("{id}")
    public String update(@PathVariable() Long id,@RequestBody Company company) {

        if ( company.getId() != null && company.getId() != id ) {
            throw new AppException("Path id does not equal Object Id!");
        }
         // TODO to return url to entity.
        companyService.update(company);
        return "ok";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        companyService.delete(id);
        return "ok";
    }

}
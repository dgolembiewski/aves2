package aves.company;

import aves.common.exception.NotFoundException;
import aves.user.User;
import aves.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    UserRepository userRepository;

    public List<Company> findAll() {
        return companyRepository.findAll();
    }

    public Company findById(Long id ) {
        return companyRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No Company with id "+id ));
    }

    public List<User> getAssociatedUsers(Long id) {

        findById(id);  // make sure it exist, if it doesn't throw notfound error
        return userRepository.findByCompanyId(id);

    }
    public Long insert( Company company ) {

        return companyRepository.save(company).getId();

    }

    public void update( Company company ) {

        companyRepository.save(company);

    }


    public String delete( Long id ) {
        
        Optional<Company> company = companyRepository.findById(id);
        if ( company.isPresent() ) {
            Company c = company.get();
            c.setIsDeleted(true);
            companyRepository.save(c);
            return "ok";
        }
        throw new NotFoundException("No Company with id "+id );

    }

      
}
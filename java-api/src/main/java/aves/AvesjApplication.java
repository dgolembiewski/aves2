package aves;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;


@SpringBootApplication
@EntityScan(basePackageClasses = {
        AvesjApplication.class,
        Jsr310JpaConverters.class
})
public class AvesjApplication {

    private static final Logger log = LoggerFactory.getLogger(AvesjApplication.class);

    public static void main(String[] args) {
        log.info("Starting...");
        SpringApplication.run(AvesjApplication.class, args);
        log.info("Started successfully.");
    }

}

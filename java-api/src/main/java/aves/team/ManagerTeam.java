package aves.team;

import aves.common.model.Auditable;
import aves.user.User;

import javax.persistence.*;

/**
 * The persistent class for the manager_team database table.
 * 
 */
@Entity
@Table(name="manager_team")
@NamedQuery(name="ManagerTeam.findAll", query="SELECT m FROM ManagerTeam m")
public class ManagerTeam extends Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	//bi-directional many-to-one association to Team
	@ManyToOne
	@JoinColumn(name="team_id", referencedColumnName="id")
	private Team team;

	//bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	private User user;

	public ManagerTeam() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
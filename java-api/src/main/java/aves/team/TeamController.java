package aves.team;

import aves.common.exception.AppException;
import aves.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/aves/api/teams")
@ResponseStatus(HttpStatus.OK)
public class TeamController {

    @Autowired
    TeamService teamService;

    @Autowired
    protected HttpServletResponse response;

    @GetMapping()
    public ResponseEntity<List<Team>> geAllTeams() {
        return new ResponseEntity<List<Team>>(teamService.findAll(), HttpStatus.OK);  
    }

    @GetMapping("{id}/childteams")
    public ResponseEntity<List<Team>> getChildTeam(@PathVariable("id") Long id) {

        return new ResponseEntity<List<Team>>(teamService.getByParentTeamId(id), HttpStatus.OK);

    }

    @GetMapping("{id}")
    public Optional<Team> getById(@PathVariable("id") Long id) {

        Optional<Team> ot = teamService.findById(id);
        if ( ot != null ) {
            return ot;
        } {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
    }

    @GetMapping("{id}/users")
    public List<User> getByIdUsers(HttpServletResponse response, @PathVariable("id") Long id) {
        return teamService.getAssociatedUsers(id);
    }


    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping()
    public String insert(@RequestBody Team team) {

        if ( team.getId() != null ) {
            throw new AppException("Insert with id is not allowed.  Do you want to update?");
        }
        // TODO to return url to entity.
        teamService.insert(team);
        return "ok";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("{id}")
    public String update(@PathVariable() Long id, @RequestBody Team team) {
        // TODO to return url to entity.

        if ( team.getId() != null && team.getId() != id ) {
            throw new AppException("Path id does not equal Object Id!");
        }
        team.setId(id);
        teamService.update(team);
        return "ok";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        teamService.deleteTeam(id);
        return "ok";
    }


}
package aves.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("aves/api/managerteams")
@ResponseStatus(HttpStatus.OK)
public class ManagerTeamController {

    @Autowired
    ManagerTeamService managerTeamService;

    @GetMapping
    public List<ManagerTeam> getAllManagerTeam() {
        return managerTeamService.findAll();
    }

    @GetMapping("{id}")
    public ManagerTeam getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return managerTeamService.findById(id);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody ManagerTeam managerTeam) {
        // TODO to return url to entity.
        managerTeamService.insert(managerTeam);
        return "ok";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("{id}")
    public String update(@RequestBody ManagerTeam managerTeam) {
         // TODO to return url to entity.
        managerTeamService.update(managerTeam);
        return "ok";
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        managerTeamService.delete(id);
        return "ok";
    }

}
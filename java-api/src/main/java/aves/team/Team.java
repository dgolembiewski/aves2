package aves.team;

import aves.common.model.Auditable;
import aves.user.User;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the team database table.
 * 
 */
@Entity
@NamedQuery(name="Team.findAll", query="SELECT t FROM Team t")
public class Team extends Auditable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name="is_deleted")
	private Boolean isDeleted;

	private String name;

	@Column(name="parent_team_id")
	private Integer parentTeamId;

	//bi-directional many-to-one association to ManagerTeam
	@OneToMany(mappedBy="team")
	@JsonIgnore
	private List<ManagerTeam> managerTeams;

	//bi-directional many-to-one association to User
	@OneToMany(mappedBy="team")
	@JsonIgnore
	private List<User> users;

	public Team() {
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Boolean getIsDeleted() {
		return this.isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getParentTeamId() {
		return this.parentTeamId;
	}

	public void setParentTeamId(Integer parentTeamId) {
		this.parentTeamId = parentTeamId;
	}

	public List<ManagerTeam> getManagerTeams() {
		return this.managerTeams;
	}

	public void setManagerTeams(List<ManagerTeam> managerTeams) {
		this.managerTeams = managerTeams;
	}

	public ManagerTeam addManagerTeam(ManagerTeam managerTeam) {
		getManagerTeams().add(managerTeam);
		managerTeam.setTeam(this);

		return managerTeam;
	}

	public ManagerTeam removeManagerTeam(ManagerTeam managerTeam) {
		getManagerTeams().remove(managerTeam);
		managerTeam.setTeam(null);

		return managerTeam;
	}

	public List<User> getUsers() {
		return this.users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User addUser(User user) {
		getUsers().add(user);
		user.setTeam(this);

		return user;
	}

	public User removeUser(User user) {
		getUsers().remove(user);
		user.setTeam(null);

		return user;
	}

}
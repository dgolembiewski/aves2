package aves.team;

import aves.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManagerTeamService {

    @Autowired
    ManagerTeamRepository managerTeamRepository;

    public List<ManagerTeam> findAll() {
        return managerTeamRepository.findAll();
    }

    public ManagerTeam findById(Long id ) {
        return managerTeamRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No ManagerTeam with id "+id ));
    }

    public Long insert( ManagerTeam managerTeam ) {
        return managerTeamRepository.save(managerTeam).getId();
    }

    public void update( ManagerTeam managerTeam ) {
        managerTeamRepository.save(managerTeam);
    }


    public String delete( Long id ) {
        
        return "not found";
    }

      
}
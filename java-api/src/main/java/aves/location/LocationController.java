package aves.location;

import aves.common.exception.AppException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


@RestController
@RequestMapping("/aves/api/locations")
@ResponseStatus(HttpStatus.OK)
public class LocationController {

    @Autowired
    LocationService locationService;

    @GetMapping
    public List<Location> getAllLocation() {
        return locationService.findAll();
    }

    @GetMapping("{id}")
    public Location getById(HttpServletResponse response, @PathVariable("id") Long id) {
        return locationService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public String insert(@RequestBody Location location) {
        if ( location.getId() != null ) {
            throw new AppException("Insert with id is not allowed.  Do you want to update?");
        }
        // TODO to return url to entity.
        locationService.insert(location);
        return "ok";
    }

    @PutMapping("{id}")
    public String update(@PathVariable() Long id,@RequestBody Location location) {

        if ( location.getId() != null && location.getId() != id ) {
            throw new AppException("Path id does not equal Object Id!");
        }
         // TODO to return url to entity.
        locationService.update(location);
        return "ok";
    }

    @DeleteMapping("{id}")
    public String delete(@PathVariable("id") Long id) {
        locationService.delete(id);
        return "ok";
    }

}
package aves.location;

import aves.common.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService {

    @Autowired
    LocationRepository locationRepository;


    public List<Location> findAll() {
        return locationRepository.findAll();
    }

    public Location findById(Long id ) {
        return locationRepository
            .findById(id)
            .orElseThrow(()->new NotFoundException("No Location with id "+id ));
    }

    public Long insert( Location location ) {
        return locationRepository.save(location).getId();
    }

    public void update( Location location ) {
        locationRepository.save(location);
    }


    public String delete( Long id ) {
        
        Optional<Location> location = locationRepository.findById(id);
        if ( location.isPresent() ) {
            Location c = location.get();
            c.setIsDeleted(true);
            locationRepository.save(c);
            return "ok";
        }
        return "not found";
    }

      
}
#!/bin/sh -e
set -e #exit script on error
cd /app
if [ -z $ANGULAR_CONFIG ]
then
    echo "ANGULAR_CONFIG not set"
else
    echo "ANGULAR_CONFIG set"
fi
echo "ANGULAR_CONFIG=${ANGULAR_CONFIG}"

folder="./src/environments"
file="${folder}/version"
version=$(cat $file)
echo "version: $version"
now=$(date)
echo "buildDate: $now"
for i in ${folder}/*.ts; do
  sed -i "s/version:.*'.*'/version: '$version'/" "$i"
  sed -i "s/buildDate:.*'.*'/buildDate: '$now'/" "$i"
done

echo "building dist folder..."
ng build --configuration=${ANGULAR_CONFIG}
cp -r dist/* /usr/share/nginx/html

echo "executing command: ${@}"
exec "$@" #execute docker command

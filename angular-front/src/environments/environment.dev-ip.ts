// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const ip = 'http://18.206.67.94';

export const environment = {
  production: false,
  version: '',
  buildDate: '',
  nodeApiUrl: ip + ':3000',
  javaApiUrl: ip + ':8080/aves/api',

  // APIs used in AVES
  loginUri: ip + ':8080/aves/api/auth/login',
  forgotPasswordUri: ip + ':8080/aves/api/auth/reset',
  registerUri: ip + ':8080/aves/api/auth/register'
};

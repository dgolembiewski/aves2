// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
const ip = '18.206.67.94';

export const environment = {
  production: false,
  version: '0.0.0',
  buildDate: new Date(),
  // ip: '18.206.67.94',
  nodeApiUrl: 'http://localhost:3000',
  javaApiUrl: 'http://localhost:8080/aves/api',

  // APIs used in AVES
  loginUri: 'http://localhost:8080/aves/api/auth/login',
  // loginUri: 'http://' + ip + ':5000/avesj/api/auth/login',
  forgotPasswordUri: 'http://localhost:8080/aves/api/auth/reset',
  // forgotPasswordUri: 'http://' + ip + ':5000/avesj/api/auth/reset',
  registerUri: 'http://localhost:8080/aves/api/auth/register'
  // registerUri: 'http://' + ip + ':5000/avesj/api/auth/register'
};

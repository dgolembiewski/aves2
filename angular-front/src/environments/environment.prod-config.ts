// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  version: '',
  buildDate: '',
  nodeApiUrl: 'http://node-express-mongo-aves-config.54.211.50.100.xip.io',
  javaApiUrl: 'http://java-api-aves-config.54.211.50.100.xip.io/aves/api',

  // APIs used in AVES
  loginUri: 'http://java-api-aves-config.54.211.50.100.xip.io/aves/api/auth/login',
  forgotPasswordUri: 'http://java-api-aves-config.54.211.50.100.xip.io/aves/api/auth/reset',
  registerUri: 'http://java-api-aves-config.54.211.50.100.xip.io/aves/api/auth/register'
};

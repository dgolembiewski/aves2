select
	u.id,
	u.first_name,
	u.last_name,
	u.email,
	u.phone,
	u.location,
	pt.id as parent_team_id,
	st.id as sub_team_id,
	c.id as company_id
from
	aves.user as u
left join aves.company as c on
	c.id = u.company
left join aves.team_membership as tm on
	u.id = tm.user_id
left join aves.team as st on
	st.id = tm.subteam_id
left join aves.team as pt on
	pt.id = st.parent_team
where
	u.id = '38f8cab1-3eb5-43c5-bef8-745fb155f23d';

SET search_path TO AVES;

CREATE TABLE IF NOT EXISTS USER_ACTIVATIONS (
	id 					SERIAL NOT NULL,
	verification_hash	varchar(255) NOT NULL,
    user_id     		uuid REFERENCES aves."user",
    created_at 			timestamp NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS STATUS_LOOKUP (
	id 					int NOT NULL,
	status				varchar(20) NOT NULL,
	description			varchar(255) NOT NULL,
    created_at 			timestamp NOT NULL,
    updated_at 			timestamp NOT NULL,
    PRIMARY KEY (id)
);

INSERT INTO STATUS_LOOKUP(id, status, description, created_at, updated_at) VALUES(1, 'ACTIVE', 'Indicates an active user in the system.', now(), now());
INSERT INTO STATUS_LOOKUP(id, status, description, created_at, updated_at) VALUES(2, 'INACTIVE', 'Indicates an inactive user in the system.', now(), now());
INSERT INTO STATUS_LOOKUP(id, status, description, created_at, updated_at) VALUES(3, 'PENDING', 'Indicates a user who is waitinng for activation.', now(), now());


ALTER TABLE aves."user"
	ADD COLUMN IF NOT EXISTS status_id int REFERENCES status_lookup (id);

ALTER TABLE aves."user"
	ADD FOREIGN KEY (status_id) REFERENCES status_lookup (id);

-- Table: aves.role

-- DROP TABLE aves.role;

CREATE TABLE aves.role
(
    id integer NOT NULL DEFAULT nextval('aves.role_id_seq'::regclass),
    name text COLLATE pg_catalog."default" NOT NULL,
    deleted boolean NOT NULL DEFAULT false,
    CONSTRAINT role_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE aves.role
    OWNER to avesadmin;

-- Added 6/27/2018 by RD

-- Constraint: aves.team_membership_team_user_key
-- Create uniqueness constraint on the membership link between user and subteam. 
-- Delete previous if exist.
ALTER TABLE aves.team_membership DROP CONSTRAINT IF EXISTS team_membership_team_user_key;

ALTER TABLE aves.team_membership
  ADD CONSTRAINT team_membership_team_user_key UNIQUE(user_id, subteam_id);

-- Constraint: aves.team_name_key
-- Create uniqueness constraint on a team name
-- Delete previous if exist.
ALTER TABLE aves.team DROP CONSTRAINT IF EXISTS team_name_key;

ALTER TABLE aves.team
  ADD CONSTRAINT team_name_key UNIQUE(name);

-- Constraint: aves.company_name_key
-- Create uniqueness constraint on a company name
-- Delete previous if exist.
ALTER TABLE aves.company DROP CONSTRAINT IF EXISTS company_name_key;

ALTER TABLE aves.company
  ADD CONSTRAINT company_name_key UNIQUE(company_name);



-- Drop table

-- DROP TABLE aves.status_lookup

CREATE TABLE aves.status_lookup (
	id serial NOT NULL,
	status varchar(20) NOT NULL,
	description varchar(255) NOT NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT status_lookup_pkey PRIMARY KEY (id),
	CONSTRAINT uk_pdib5ui1dx9t1ecwfcsp5jsep UNIQUE (status)
)
WITH (
	OIDS=FALSE
) ;

-- DROP TABLE aves.role;

CREATE TABLE aves."role" (
	id serial NOT NULL,
	description varchar(255) NULL DEFAULT NULL::character varying,
	"name" varchar(255) NULL DEFAULT NULL::character varying,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT role_pkey PRIMARY KEY (id),
	CONSTRAINT uk_epk9im9l9q67xmwi4hbed25do UNIQUE (name)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE aves.role
    OWNER to avesadmin;


-- Drop table

-- DROP TABLE aves.user_role

CREATE TABLE aves.user_role (
	user_id uuid NOT NULL,
	role_id int4 NOT NULL,
	CONSTRAINT fk859n2jvi8ivhui0rl0esws6o FOREIGN KEY (user_id) REFERENCES aves."user"(id),
	CONSTRAINT fka68196081fvovjhkek5m97n3y FOREIGN KEY (role_id) REFERENCES aves.role(id)
)
WITH (
	OIDS=FALSE
) ;

-- ALTER TABLE aves."user" ADD lastlogin timestamp NOT NULL;
-- ALTER TABLE aves."user" ADD created_at timestamp NOT NULL;
-- ALTER TABLE aves."user" ADD updated_at timestamp NOT NULL;
-- ALTER TABLE aves."user" ADD username text NULL;
-- ALTER TABLE aves."user" ADD disabled bool NULL;
-- ALTER TABLE aves."user" ADD userroles text NULL;
-- ALTER TABLE aves."user" ADD password text NULL;

ALTER TABLE aves."user" ADD status_id int4 NULL;

-- Drop table

-- DROP TABLE aves.user_activations

CREATE TABLE aves.user_activations (
	id serial NOT NULL,
	verification_hash varchar(255) NOT NULL,
	user_id uuid NULL,
	created_at timestamp NOT NULL,
	updated_at timestamp NOT NULL,
	CONSTRAINT user_activations_pkey PRIMARY KEY (id),
	CONSTRAINT user_activations_user_id_fkey FOREIGN KEY (user_id) REFERENCES aves."user"(id)
)
WITH (
	OIDS=FALSE
) ;

CREATE TABLE IF NOT EXISTS USER_ACTIVATIONS (
	id 					SERIAL NOT NULL,
	verification_hash	varchar(255) NOT NULL,
    user_id     		uuid REFERENCES aves."user",
    created_at 			timestamp NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS STATUS_LOOKUP (
	id 					int NOT NULL,
	status				varchar(20) NOT NULL,
	description			varchar(255) NOT NULL,
    created_at 			timestamp NOT NULL,
    updated_at 			timestamp NOT NULL,
    PRIMARY KEY (id)
);


ALTER TABLE aves."user"
	ADD FOREIGN KEY (status_id) REFERENCES status_lookup (id);

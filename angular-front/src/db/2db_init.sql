CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

-- DROP SCHEMA aves;

CREATE SCHEMA aves AUTHORIZATION avesadmin;

COMMENT ON SCHEMA aves IS 'standard public schema';

-- Table: aves.company

-- DROP TABLE aves.company;

CREATE TABLE aves.company
(
    id SERIAL PRIMARY KEY,
    company_name text COLLATE pg_catalog."default" NOT NULL,
    deleted boolean NOT NULL DEFAULT false
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE aves.company
    OWNER to avesadmin;

-- Table: aves.team

-- DROP TABLE aves.team;

CREATE TABLE aves.team
(
    id SERIAL PRIMARY KEY,
    name text COLLATE pg_catalog."default" NOT NULL,
    parent_team integer,
    deleted boolean NOT NULL DEFAULT false,
    CONSTRAINT team_team_fk FOREIGN KEY (parent_team)
        REFERENCES aves.team (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE aves.team
    OWNER to avesadmin;

-- Table: aves."user"

-- DROP TABLE aves."user";

CREATE TABLE aves."user"
(
    id uuid default uuid_generate_v4() PRIMARY KEY,
    first_name text COLLATE pg_catalog."default",
    last_name text COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default",
    phone text COLLATE pg_catalog."default",
    location text COLLATE pg_catalog."default",
    company integer,
    position text,
    deleted boolean NOT NULL DEFAULT false,
    CONSTRAINT user_email_key UNIQUE (email),
    CONSTRAINT company_id_fk FOREIGN KEY (company)
        REFERENCES aves.company (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE aves."user"
    OWNER to avesadmin;

-- Index: fki_company-id_fk

-- DROP INDEX aves."fki_company-id_fk";

CREATE INDEX "fki_company-id_fk"
    ON aves."user" USING btree
    (company)
    TABLESPACE pg_default;


-- Table: aves.team_members

-- DROP TABLE aves.team_members;

CREATE TABLE aves.team_membership
(
    id SERIAL PRIMARY KEY,
    subteam_id integer NOT NULL,
    user_id uuid NOT NULL,
    deleted boolean NOT NULL DEFAULT false,
    CONSTRAINT subteam_id FOREIGN KEY (subteam_id)
        REFERENCES aves.team (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES aves."user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE aves.team_membership
    OWNER to avesadmin;

-- Index: fki_subteam_id

-- DROP INDEX aves.fki_subteam_id;

CREATE INDEX fki_subteam_id
    ON aves.team_membership USING btree
    (subteam_id)
    TABLESPACE pg_default;

-- Index: fki_user_id_fk

-- DROP INDEX aves.fki_user_id_fk;

CREATE INDEX fki_user_id_fk
    ON aves.team_membership USING btree
    (user_id)
    TABLESPACE pg_default;

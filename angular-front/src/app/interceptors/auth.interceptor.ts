import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>,
        next: HttpHandler): Observable<HttpEvent<any>> {

            const token = localStorage.getItem(`id_token`);
            if (token) {
                const clonedRequest = req.clone({
                    headers: req.headers.set(`Authorization`, `Bearer ` + token)
                });
                return next.handle(clonedRequest);
            } else {
                return next.handle(req);
            }
        }
}

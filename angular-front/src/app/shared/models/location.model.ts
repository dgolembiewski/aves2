export class Location {
    id: number;
    addressLine1: string;
    addressLine2: string;
    city: string;
    name: string;
    state: string;
    zipcode: number;
    isDeleted: boolean;
}
export class Company {
    id: number; // corresponds with companyId from Teammate
    name: string;
    isDeleted: boolean;

    constructor() {
        this.id = null;
        this.name = '';
        this.isDeleted = false;
    }
}
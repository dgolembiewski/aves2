export class Member {

    src_id: string;
    _id: string;
    name: string;
    skills: any[];
    bio: string;
    photo: string;
  
    constructor() { }
  
  }
  
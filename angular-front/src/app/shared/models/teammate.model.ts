import {Location} from './location.model';
import {Company} from './company.model';
import {Position} from './position.model';
import {Team} from './team.model';

export class Teammate {
    id: string; // corresponds with src_id from MemberComponent
    firstName: string;
    lastName: string;
    nickName: string;
    bio: string;
    email: string;
    phone: string;
    location: Location;
    position: Position;
    company: Company;
    isDeleted: boolean;
    photo: string;
    editable: boolean;
    isFreeAgent: boolean;
    availableDate: string;
    skills: Array<{}>;
    team: Team;

    constructor() {
        this.id = '';
        this.nickName = '';
        this.bio = '';
        this.email = '';
        this.phone = '';
        this.location = null;
        this.position = null;
        this.company = null;
        this.isDeleted = false;
        this.photo = 'https://www.w3schools.com/howto/img_avatar.png';
        this.editable = false;
        this.isFreeAgent = true;
        this.availableDate = null;
        this.skills = null;
        this.team = null;
    }
}

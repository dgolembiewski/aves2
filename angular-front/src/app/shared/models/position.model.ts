export class Position {
    id: number;
    name: string;
    isDeleted: boolean;
}

export class Team {
    id: number; // takes on {1,2,3,4,5,6,7,8,9,10}
    name: string;
    parentTeamId: number; // takes on {1,5,8,null}
    isDeleted: boolean;

    constructor() {
        this.id = null;
        this.name = '';
        this.parentTeamId = null;
        this.isDeleted = false;
    }
}
import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import 'rxjs/add/operator/shareReplay';
import 'rxjs/add/operator/do';

import { environment } from '../../environments/environment';

// import {Company} from '../components/company/company';
// import { Member } from '../components/members/member/member';
import { SkillsComponent } from '../components/skills/skills.component';
import { Task } from '../components/tasks/task/task';
import {Team} from '../components/team/team';
// import {Teammate} from '../components/teammate/teammate';

import {Location} from '../shared/models/location.model';
import {Company} from '../shared/models/company.model';
import {Position} from '../shared/models/position.model';
import {Teammate} from '../shared/models/teammate.model';
import {Member} from '../shared/models/member.model';



const javaServerWithApi = environment.javaApiUrl; // + environment.javaApiPath;
const httpOptions = {
  headers: new HttpHeaders({'Accept': 'application/json'})
};

@Injectable({providedIn: 'root'})

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  companiesUrl = javaServerWithApi + '/companies';
  loginUrl = javaServerWithApi + '/auth/login';
  //membersUrl = javaServerWithApi + '/team/{teamid}/user/{userid}';  //TODO: ????
  membersUrl = environment.nodeApiUrl + '/api/members';
  passwordResetUrl = javaServerWithApi + '/auth/reset';
  passwordUpdateUrl = javaServerWithApi + '/auth/updatePassword';
  registerUserUrl = javaServerWithApi + '/auth/register';
  rolesUrl = javaServerWithApi + '/roles';
  skillsUrl = environment.nodeApiUrl + '/skills'; //TODO: ????
  tasksUrl = javaServerWithApi + '/tasks'; //TODO: ????
  teamsUrl = javaServerWithApi + '/teams';
  teammatesUrl = javaServerWithApi + '/users';
  nodePost = javaServerWithApi + '/apijava'; //TODO: ????
  auth_base_url = javaServerWithApi + '/auth';
  headers = new HttpHeaders({'Content-Type': 'application/json'});
  
  //For Caching
  teammates: Observable<Teammate[]>;
  companies: Observable<Company[]>;
  members: Observable<Member[]>;
  //This is the wrong way of doing it probably
  emptyTeammates: Observable<Teammate[]>;
  emptyCompanies: Observable<Company[]>;
  emptyMembers: Observable<Member[]>;
  //For infinite-scrolling
  teammatesArray: Teammate[];
  companiesArray: Company[];
  membersArray: Member[];

  cacheTimeMs = 3600000; //Default 1 Hour

  constructor(private http: HttpClient) { }



  /***** AUTH REST API SERVICE BRGIN   *****/

  //Create new user - register user
  postUser(firstName:string, lastName:string, username:string, email:string, password:string):Observable<any> {
    const obj = {
      email: email,
      firstName: firstName,
      lastName: lastName,
      password: password,
      username: username
    };

    return this.http.post(this.registerUserUrl, obj)
              //   .map(res => {
              //   return res;
              // })
              // .catch(err => {
              //   return Observable.throw(err);
              // });
  }

  // Login - submit login credentials and retrieve response
  getLogin(username: string, password: string): Observable<any> {
    const obj = {
      password: password,
      usernameOrEmail: username
    };

    return this.http.post(this.loginUrl, obj)
      .do(res => this.setSession)
      .shareReplay();
            //  .map(res => {
            //    return res;
            //  })
            //  .catch(err => {
            //    return Observable.throw(err);
            //  });
  }

  //Forgot Password - Reset password request - submit account information and retrieve response
  getAccountInfo(usernameOrEmail:string):Observable<any> {
    const obj = {
      usernameOrEmail: usernameOrEmail
    };

    return this.http.post(this.passwordResetUrl, obj)
            //  .map(res => {
            //    return res;
            //  })
            //  .catch(err => {
            //    return Observable.throw(err);
            //  });
  }

  updatePassword(token:string, password: string): Observable<any> {
    var obj = {
      token : token,
      newPassword : password
    };
        return this.http.post(this.passwordUpdateUrl, obj)
           // .map(res =>{
           //   return res;
           // })
           // .catch(err => {
           //   return Observable.throw(err);
           // });
  }

  /***** AUTH REST API SERVICE END   *****/

  /***** AUTH FOR PASSWORD/EMAIL ??  *****/

  checkUserNameAvailability(username: string): Observable<any> {
    const url = this.auth_base_url + '/checkUsernameAvailability';
    const params = new HttpParams().set('username', username);

    return this.http.get(url, {headers: this.headers, params: params});
  }

  checkEmailAvailability(email: string): Observable<any> {
    const url = this.auth_base_url + '/checkEmailAvailability';
    const params = new HttpParams().set('email', email);

    return this.http.get(url, {headers: this.headers, params: params});
  }

  register(signUpRequest): Observable<any> {
    const url = this.auth_base_url + '/register';

    return this.http.post(url, signUpRequest, {headers: this.headers});
  }

  // login(loginRequest) {
  //   const url = this.auth_base_url +  '/login';

  //   return this.http.post(url, loginRequest, {headers: this.headers})
  //     .do(res => this.setSession)
  //     .shareReplay();
  // }

  reset(resetRequest): Observable<any> {
    const url = this.auth_base_url + '/register';

    return this.http.post(url, resetRequest, {headers: this.headers});
  }

  validateResetPasswordToken(resetToken): Observable<any> {
    const url = this.auth_base_url + '/validateToken';

    return this.http.post(url, resetToken, {headers: this.headers});
  }

  updatePassword2(passwordUpdateRequest): Observable<any> {
    const url = this.auth_base_url + '/updatePassword';

    return this.http.post(url, passwordUpdateRequest, {headers: this.headers});
  }

  private setSession(authResult) {
    localStorage.setItem('id_token', authResult.accessToken);
  }

  /***** AUTH FOR PASSWORD/EMAIL ??  END *****/



  /***** COMPANIES REST API SERVICE BEGIN *****/

  // get all companies
  getCompanies(): Observable<Company[]> {
    if ((this.companies == undefined)) {
      console.log('NO COMPANIES CACHED. GRABBING COMPANIES.');
      this.companies = this.http.get<Company[]>(this.companiesUrl);
      setTimeout( () => { this.companies = this.emptyCompanies; }, this.cacheTimeMs);
    } else {
      console.log('COMPANIES CACHED. SKIPPING GRABBING COMPANIES.');
    }
    return this.companies;
  }

  // getCompaniesBySection(indexStart: number, indexEnd: number): Observable<Company[]> {
  getCompaniesBySection(indexStart: number, indexEnd: number): Company[] {
      var tempCompanies;
    if (indexStart > indexEnd) {
      var temp = indexEnd;
      indexStart = indexEnd;
      indexEnd = temp;
    }
    if (indexStart < 0) {
      indexStart = 0;
    } 
    if (indexEnd > this.companiesArray.length) {
      indexEnd = this.companiesArray.length;
    }
    tempCompanies = this.companiesArray.splice(indexStart, indexEnd);
    console.log('Returning this for companies: ' + tempCompanies);
    // return of(tempCompanies);
    return tempCompanies;
  }

  // get specific company by company ID
  getCompanyById(id: number): Observable<Company> {
    return this.http.get<Company>(`this.companiesUrl/${id}`);
  }

  //Sets array in companies for infinite scrolling
  updateCompaniesArray(cb) {
    this.getCompanies()
    .subscribe(companies => {
      this.companiesArray = companies;
      return cb();
    });
  }

  /***** COMPANIES REST API SERVICE END *****/



  /***** MEMBER REST API SERVICE BEGIN *****/

  getMembers (): Observable<Member[]> {
    if (this.members == undefined) {
      console.log('NO MEMBERS CACHED. GRABBING MEMBERS.');
      this.members = this.http.get<Member[]>(this.membersUrl);
      setTimeout( () => { this.members = this.emptyMembers; }, this.cacheTimeMs);
    } else {
      console.log('MEMBERS CACHED. SKIPPING GRABBING MEMBERS.');
    }
    return this.members;
  }

  // getMembersBySection(indexStart: number, indexEnd: number): Observable<Member[]> {
  getMembersBySection(indexStart: number, indexEnd: number): Member[] {
      var tempMembers;
    
    if (indexStart > indexEnd) {
      var temp = indexEnd;
      indexStart = indexEnd;
      indexEnd = temp;
    }
    if (indexStart < 0) {
      indexStart = 0;
    } 
    if (indexEnd > this.membersArray.length) {
      indexEnd = this.membersArray.length;
    }
    tempMembers = this.membersArray.splice(indexStart, indexEnd);
    console.log('Returning this for members: ' + tempMembers);
    // return of(tempMembers);
    return tempMembers;
  }

  getMemberBySrc (src_id: string): Observable<Member> {
    return this.http.get<Member>(`${this.membersUrl}/src/${src_id}`);
  }

  updateMemberBySrc (member): Observable<any> {
    return this.http.put(`${this.membersUrl}/src/${member.src_id}`, member);
  }

  addNewMember (member): Observable<any> {
    return this.http.post<Member>(this.membersUrl, member)
  }

  //Sets array in members for infinite scrolling
  // updateMembersArray(): void {
  updateMembersArray(cb) {
    this.getMembers()
    .subscribe(members => {
      this.membersArray = members;
      return cb();
    });
  }

  /***** MEMBER REST API SERVICE END *****/



  /***** COMPANIES REST API SERVICE BEGIN *****/

  // get all roles
  /*getRoles(): Observable<Roles[]> {
    return this.http.get<Roles[]>(this.rolesUrl);
  }

  // get specific role by role id
  getRoleById(id: number): Observable<Roles> {
    return this.http.get<Company>(`this.rolesUrl/${id}`);
  }*/

  /***** COMPANIES REST API SERVICE END *****/




  /***** SKILLS REST API SERVICE BEGIN */

  getSkills(): Observable<SkillsComponent[]> {
    return this.http.get<SkillsComponent[]>(this.skillsUrl);
  }

  /***** SKILLS REST API SERVICE END */





  /***** TASKS REST API SERVICE BEGIN */

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>(this.tasksUrl)
  }

  /***** TASKS REST API SERVICE END */



  /***** TEAMS REST API SERVICE BEGIN    *****/
  // get all teams
  getTeams(): Observable<Team[]> {
    return this.http.get<Team[]>(this.teamsUrl);
  }

  // get specific team by team ID
  getTeamById(id: number): Observable<Team> {
    const getUrl = `${this.teamsUrl}/${id}`;
    return this.http.get<Team>(getUrl);
  }

  /***** TEAMS REST API SERVICE END    *****/





  /***** TEAMMATE REST API SERVICE BEGIN    *****/

  //get all teammates
  getTeammates(): Observable<Teammate[]> {
    if (this.teammates == undefined) {
      console.log('NO TEAMMATES CACHED. GRABBING TEAMMATES.');
      this.teammates = this.http.get<Teammate[]>(this.teammatesUrl);
      setTimeout( () => { this.teammates = this.emptyTeammates; }, this.cacheTimeMs);
    } else {
      console.log('TEAMMATES CACHED. SKIPPING GRABBING TEAMMATES.');
    }
    return this.teammates;
  }

  //run update/get request before this otherwise it errors out
  getTeammatesBySection(indexStart: number, indexEnd: number): Teammate[] {
    var tempTeammates = [];
    if (this.teammates == undefined) {
      return null;
    }
    
    try {
      if (indexStart >= this.teammatesArray.length) {
        return tempTeammates;
      } 
      if (indexStart > indexEnd) {
        var temp = indexEnd;
        indexStart = indexEnd;
        indexEnd = temp;
      }
      if (indexStart < 0) {
        indexStart = 0;
      } 
      if (indexEnd > this.teammatesArray.length) {
        indexEnd = this.teammatesArray.length;
      }
      tempTeammates = this.teammatesArray.splice(indexStart, indexEnd);
    } catch (err) {
      console.log('Error in teammates by section ' + err);
    } 
    return tempTeammates;
  }

  // get specific teammate by user ID
  getTeammateById(id: string): Observable<Teammate> {
    const getUrl = `${this.teammatesUrl}/${id}`;
    return this.http.get<Teammate>(getUrl);
  }

  // get teammates whose name contains search term
  searchTeammates(term: string): Observable<Teammate[]> {
    if (!term.trim()) {
      // if no search term was submitted, return an empty teammate array
      return of([]);
    }

//   return this.http.get<Teammate[]>(`${this.nodePost}/userSearch?name=${term}`);
    return this.http.get<Teammate[]>(`${environment.nodeApiUrl}/apijava/userSearch?name=${term}`);
  }

  // delete specific teammate by user ID
  deleteTeammate(id: string): Observable<Teammate> {
    const deleteUrl = `${this.teammatesUrl}/${id}`;
    return this.http.delete<Teammate>(deleteUrl);
  }

  addNewTeammate (teammate): Observable<any> {
    const obj = {
      firstName: teammate.firstName,
      lastName: teammate.lastName,
      phone: teammate.phone,
      location: teammate.location,
      position: teammate.position
    };

    return this.http.post<Teammate>(this.teammatesUrl, obj);
  }

  updateTeammate (teammate): Observable<any> {
    const obj = {
      firstName: teammate.firstName,
      lastName: teammate.lastName,
      phone: teammate.phone,
      location: teammate.location,
      position: teammate.position
    };

    return this.http.put(`${this.teammatesUrl}/${teammate.id}`, teammate, {responseType: 'text'});
  }

  //Sets array in teammates for infinite scrolling
  // updateTeammatesArray(cb)  {
  updateTeammatesArray(cb) {
    this.getTeammates()
    .subscribe(teammates => {
      this.teammatesArray = teammates;
      return cb();
    });
    
 
  }

  /***** TEAMMATE REST API SERVICE END    *****/





}

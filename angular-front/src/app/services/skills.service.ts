import { Injectable } from '@angular/core';
import { SkillsComponent } from '../components/skills/skills.component';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

// import { Skills } from "../components/skills/skills"

/// special header in HTTP save requests
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class SkillsService {

  skillGet = '/skill?id=';
  //  ADD CODE TO INCLUDE SERVICE FOR NODE

  constructor(
    private http: HttpClient,
  ) { }

  public skillsUrl = environment.nodeApiUrl + '/apijava/skills';




  getSkills(): Observable<SkillsComponent[]> {
    return this.http.get<SkillsComponent[]>(this.skillsUrl);
  }

  getSkillsUsersInfo(skill: string): Observable<any> {
    /*     const getUrl = `${this.skillsUrl}${this.skillGet}${this.skillsUrl}` */
    return this.http.get<SkillsComponent>(`${this.skillsUrl}/${skill}`)
  }

}



/////////////////////////////////

/* 
import { Observable, of } from 'rxjs'; */

/* import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators'; */

/* import { MembersComponent } from '../components/members/members.component';
import { MemberComponent } from '../components/member/member.component';
 */
//import { environment } from '../../environments/environment';

/// special header in HTTP save requests
/* const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
}; */
/* 
@Injectable({
  providedIn: 'root'
})

export class MemberService {

  constructor(
    private http: HttpClient,
  ) { }

  public membersUrl = environment.nodeApiUrl + '/api/members';

  getMembers(): Observable<MemberComponent[]> {
    return this.http.get<MemberComponent[]>(this.membersUrl);
  }

  getMemberBySrc(src_id: string): Observable<MemberComponent> {
    return this.http.get<MemberComponent>(`${this.membersUrl}/src/${src_id}`);
  }

  updateMemberBySrc(member): Observable<any> {
    return this.http.put(`${this.membersUrl}/src/${member.src_id}`, member);
  }

  addNewMember(member): Observable<any> {
    return this.http.post<MemberComponent>(this.membersUrl, member)
  }

} */

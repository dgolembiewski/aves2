import { Component, OnInit } from '@angular/core';
//import { Router } from '@angular/router';
import { environment } from '../environments/environment.prod';

import { RestApiService } from './services/restapi.service'
import { LoginComponent } from './components/login/login.component'


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  version = environment.version;
  buildDate = environment.buildDate;
  // loginUser: LoginComponents[];

  constructor(private loginService: RestApiService) { }

  ngOnInit() {
    // this.getUser();
  }

  checkLogin(): boolean {
    if (localStorage.getItem('user') == 'true') {
      return true;
    } else {
      return false;
    }
  }

  // getUser(): void {
  //   this.loginService.getName()
  //     .subscribe(loginUsers => {
  //       this.loginUsers = loginUsers;
  //       console.log('this is login Users', loginUsers);
  //     })
  // }

  logout(): void {
    localStorage.removeItem('user');
//    this.router.navigate(['members']);
  }
}

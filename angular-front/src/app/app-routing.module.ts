import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CovalentLayoutModule } from '@covalent/core/layout';
import { CovalentStepsModule  } from '@covalent/core/steps';
import { MatDialogModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatRippleModule } from '@angular/material';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { PasswordResetComponent } from './components/password-reset/password-reset.component';

import { FileSelectDirective } from 'ng2-file-upload';

const appRoutes: Routes = [
    {
        path: 'communications',
        loadChildren: './components/communications/communications.module#CommunicationsModule'
    },
    {
        path: 'orders',
        loadChildren: './components/orders/orders.module#OrdersModule'
    },
    {
        path: 'tasks',
        loadChildren: './components/tasks/tasks.module#TasksModule'
    },
    {   path: 'member-search',
        loadChildren: './components/member-search/member-search.module#MemberSearchModule'
    },
    {
        path: 'members',
        loadChildren: './components/members/members.module#MembersModule'
    },
    {
        path: 'login',
        loadChildren: './components/login/login.module#LoginModule'
    },
    {
        path: 'new-user',
        loadChildren: './components/new-user/new-user.module#NewUserModule'
    },
    {
        path: 'adduser',
        loadChildren: './components/add-user/add-user.module#AddUserModule'
    },
    {
        path: 'forgot-password',
        loadChildren: './components/forgot-password/forgot-password.module#ForgotPasswordModule'
    },
    {
        path: 'reset_password/:token',
        // loadChildren: './components/password-reset/password-reset.module#PasswordResetModule'
        component: PasswordResetComponent
    },
    {
        path: 'skills',
        loadChildren: './components/skills/skills.module#SkillsModule'

    },

  ];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
//     entryComponents: [
//     ModalWindow
//   ],
})
export class AppRoutingModule {}

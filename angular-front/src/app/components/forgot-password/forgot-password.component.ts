import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { RestApiService } from '../../services/restapi.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})

export class ForgotPasswordComponent implements OnInit {
  forgotPasswordMessage:string;

  constructor(
    public appComponent:AppComponent, 
    private restApiService: RestApiService)
    { }

  ngOnInit() {
  }

  //retrieve data from form and check if information is from a valid user
  onClickSubmit(data):void {
    this.restApiService.getAccountInfo(data.usernameOrEmail).subscribe(res => {
      this.forgotPasswordMessage = 'Request has been submitted.';
    },
    err => {
      console.log(err);
      this.forgotPasswordMessage = 'Username or email can not be found!';
    });
  }
}

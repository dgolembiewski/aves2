import { Component, OnInit } from '@angular/core';

import { Member } from '../members/member/member';
import { Teammate } from '../teammate/teammate';
import { Company } from '../company/company';

// import { MemberService } from '../../services/member.service'
// import { TeammateService } from '../../services/teammate.service';
// import { CompanyService } from '../../services/company.service';
// import { SkillsService } from '../../services/skills.service';
import { RestApiService } from '../../services/restapi.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.css']
})
export class SkillsComponent implements OnInit {

  //skilledPerson: SkilledPerson[];
  teammates: Teammate[];
  members: Member[];
  skills: SkillsComponent[];

  combined = [];
  AllSkills = [];
  uniqueSkills = [];

  constructor(
    // private skillService: SkillsService, 
    // private memberService: MemberService, 
    // private teammateService: TeammateService, 
    // private companyService: CompanyService) { }
    private restApiService: RestApiService) { }

  ngOnInit() {
    // this.getTeammates();
    this.getSkills(); 
    console.log("Hello");
    }


  getSkills(): void {
    this.restApiService.getSkills()
      .subscribe(skills => {
        this.skills = skills;
        this.getSkillList();

      });
  }



/*
getSkillsUsersInfo(skill): void {
  this.skillService.getSkillsUsersInfo(skill).subscribe(skilledPerson => {
    this.skilledPerson = skilledPerson;
    this.

  })


}
*/



  // getTeammates(): void {
  //   this.teammateService.getTeammates()
  //     .subscribe(teammates => {
  //       this.teammates = teammates;
  //       this.getMembers();
  //     });
  // }

  getMembers(): void {
    this.restApiService.getMembers()
      .subscribe(members => {
        this.members = members;
        this.combineData()
      });
  }


  getSkillList(): void {    
    this.skills.forEach( skill => this.uniqueSkills.push(skill))

  }

  //getTEAMS WHEN AVAILABLE
  combineData(): void {

    this.teammates.forEach(teammate => this.combined.push(teammate))

    this.combined.forEach(combinee => {

      let matchIdx = this.members.findIndex(member => {
        return member.src_id === combinee.id;
      });

      if (matchIdx >= 0) {
        combinee.id = this.members[matchIdx].src_id;

        combinee.name = 'XXX';
        if (this.teammates[matchIdx].firstName) {
          combinee.name = this.teammates[matchIdx].firstName + " ";
        }
        else {
          combinee.name = "(NO FIRST NAME) ";
        }
        if (this.teammates[matchIdx].lastName) {
          combinee.name = combinee.name + this.teammates[matchIdx].lastName + " ";
        }
        else {
          combinee.name = combinee.name + " (NO LAST NAME)";
        }

        //combinee.team = // add TEAM WHEN AVAILABLE
        combinee.skills = this.members[matchIdx].skills;

        // get list of all skills
        combinee.skills.forEach(skill => {
          var item;
          if (skill.title)
            item = skill.title.toUpperCase();
          else
            item = skill.title;

          this.AllSkills.push(item);
        })


        /*         this.uniqueSkills = this.AllSkills.filter((x, i, a) => a.indexOf(x) == i);
                console.log(this.uniqueSkills); */

        this.uniqueSkills = this.AllSkills.reduce(function (acc, curr) {
          if (typeof acc[curr] == 'undefined') {
            acc[curr] = 1;
          } else {
            acc[curr] += 1;
          }

          return acc;
        }, []).sort((n1, n2) => n1 > n2);




        console.log(this.uniqueSkills);

        /* var x = Array();
               for (let i=0; i< this.AllSkills.length; i++) 
                {
                 if(x.co )   
        
                } 
         */

        //add team when available

        // no match
      } else if (matchIdx === -1) {
        combinee.name = "XXX -NO NAME- MISMATCHED- XXX"
        combinee.skills = [{ title: "n/a", rating: 0 }]
        // combinee.skills = [{ title: "no match found", rating: 0 }];

      }




      //combined has all users/member data for name, skills 
      //combined.name will print out name


      //loop through all users

      // loop through all skills of that user
      //skill.title  push into 
      //skill.rating

      //store user[i][x][y]


    });
  }

}

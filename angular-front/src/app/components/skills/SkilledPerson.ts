export class SkilledPerson {
    id: string; // corresponds with src_id from MemberComponent
    firstName: string;
    fname: string;
    lastName: string;
    lname: string;
    email: string;
    phone: string;
    location: string;
    position: string;
    company_name: number; // corresponds with id from Company
    companyId: number;
    deleted: boolean;
    imageSrc: string;
    editable: boolean;
    freeAgent: boolean;
    availableDate: Date;

    constructor() {
        this.id = '';
        this.fname = '';
        this.lname = '';
        this.email = '';
        this.phone = '';
        this.location = '';
        this.position = '';
        this.companyId = null;
        this.deleted = false;
        this.imageSrc = 'https://www.w3schools.com/howto/img_avatar.png';
        this.editable = false;
        this.freeAgent = true;
        this.availableDate = null;
    }
}

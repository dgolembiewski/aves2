import { Component, OnInit, Input, NgModule, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

// import { Teammate } from '../teammate/teammate';
// import { Company } from '../company/company';
// import { Member } from '../members/member/member';

import {Company} from '../../shared/models/company.model';
import {Position} from '../../shared/models/position.model';
import {Teammate} from '../../shared/models/teammate.model';
import {Member} from '../../shared/models/member.model';

import { RestApiService } from '../../services/restapi.service';

import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../../environments/environment';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-member-detail',
  templateUrl: './member-detail.component.html',
  styleUrls: ['./member-detail.component.css']
})

export class MemberDetailComponent {

  @ViewChild('f') form;

  teammate: Teammate;
  company: Company;
  member: Member;
  minDate: String;
  attachmentList: any = [];
  public membersUrl = environment.nodeApiUrl + '/api/upload';
  companies:  Company[];

  public uploader: FileUploader = new FileUploader({ url: this.membersUrl });

  public saveSuccess: boolean = false;
  public saveFailure: boolean = false;
  public skillTitleError: boolean = false;
  public skillRatingError: boolean = false;
  public saveMessage: string;
  public defaultPicture: boolean = false;

  constructor(private route: ActivatedRoute,
    private restApiService: RestApiService,
    private location: Location) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      const my_id = params.get('id');
      this.getTeammate(my_id);
      this.getCompanies();
    });
    this.uploader.onAfterAddingFile = (file) => { file.withCredentials = false; };
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
      console.log('ImageUpload:uploaded:', item, status, response);
    };
    const availDate = new Date();
    availDate.setDate(availDate.getDate() + 1);
    this.minDate = availDate.toISOString().slice(0, 10);
  }

  download(index) {
    var filename = this;
  }

  getTeammate(id): void {
    this.restApiService.getTeammateById(id).subscribe(teammate => {
      this.teammate = teammate;
      // this.teammate.firstName = teammate.fname;
      // delete this.teammate.fname;
      // this.teammate.lastName = teammate.lname;
      // delete this.teammate.lname;
      // this.teammate.company_name = teammate.companyId;
      // delete this.teammate.companyId;
      // this.getCompany();
      this.getMember();
      console.log(this.teammate);
    });
  }

  getCompanies(): void {
    this.restApiService.getCompanies()
    .subscribe(companies => {
      this.companies = companies;
      console.log(this.companies);
    });
  }

  getCompany(): void {
    this.restApiService.getCompanyById(this.teammate.company.id).subscribe(company => {
      this.company = company;
      this.getMember();
    });
  }

  getMember(): void {
    const src_id = this.route.snapshot.paramMap.get('id');
    this.restApiService.getMemberBySrc(src_id)
      .subscribe(member => {
        this.member = member;
      });
  }

  uploadPicture(event): void {
    let image = (<HTMLInputElement>document.querySelector('input[name=imageUpload]')).files[0];
    let reader = new FileReader();

    //if an image is uploaded, will read the contents of that image
    if (image) {
      reader.readAsDataURL(image);
    }

    //doing this so that i will be able to access member.photo without losing scope
    var that = this

    //only happens if a file is uploaded
    reader.onload = function () {
      var base64result = reader.result
      that.member.photo = base64result
      console.log(that.member.photo)
    };
  }

  //replaces uploaded image with stock photo
  deletePicture(): void {
    this.defaultPicture = true
  }

  print(val): void {
    // console.log(val)
    // console.log(this.member)
  }

  addSkill(): void {
    this.member.skills.push({});
  }

  removeSkill(indx): void {
    this.member.skills.splice(indx, 1);
  }

  goBack(): void {
    this.location.back();
  }

  availChangeHandler(): void {
    if (this.teammate.isFreeAgent) {
      this.teammate.availableDate = new Date().toISOString().slice(0, 10);
    }
  }

  compareFn(c1: Company, c2: Company): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
}


  onUpdate(data): void {
    this.saveSuccess = false;
    this.skillRatingError = false;
    this.skillTitleError = false;

    // Validation for skill Title and skill Radio button selection handled here (other validations moved to view)
    var skillTitleInputs = document.getElementsByClassName("skillTitleInput");
    for(var i = 0; i < skillTitleInputs.length; i++)
    {
      var skillTitle = skillTitleInputs[i].getAttribute('ng-reflect-model');
      if(skillTitle == null || !skillTitle.replace(/\s/g, '').length)
      {
        this.skillTitleError = true;
      }
    }

    var skillRadioInalidInputs = document.getElementsByClassName("skillRatingRadio ng-invalid");
    if (skillRadioInalidInputs.length > 0)
    {
      this.skillRatingError = true;
    }

    if(this.skillRatingError == false && this.skillTitleError == false)
    {
      this.restApiService.updateTeammate(this.teammate).subscribe();
      this.restApiService.updateMemberBySrc(this.member)
      .subscribe(member =>
      {
        this.saveMessage = `member with src_id : \n\t${member.src_id}\n has been updated.`;
        this.saveSuccess = true;
      });
      if(this.defaultPicture)
      {
        this.member.photo = 'https://www.w3schools.com/howto/img_avatar.png';
      }
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { MemberSearchRoutingModule } from './member-search-routing.module';
import { MemberSearchComponent } from './member-search.component';

@NgModule({
  imports: [
    CommonModule,
    MemberSearchRoutingModule,
    FormsModule
  ],
  declarations: [
    MemberSearchComponent
  ],
  exports: [
    MemberSearchComponent
  ]
})
export class MemberSearchModule { }

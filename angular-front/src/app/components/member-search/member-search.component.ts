import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
//import { Teammate } from '../teammate/teammate';
import {Teammate} from '../../shared/models/teammate.model';
// import { TeammateService } from '../../services/teammate.service';
import { RestApiService } from '../../services/restapi.service';

@Component({
  selector: 'app-member-search',
  templateUrl: './member-search.component.html',
  styleUrls: ['./member-search.component.css']
})
export class MemberSearchComponent implements OnInit {
  teammates$: Observable<Teammate[]>;
  private searchTerms = new Subject<string>();
//  view: string = 'condensed'; // determine if a condensed view of the search is displayed

  constructor(private restApiService: RestApiService) {}

  ngOnInit(): void {
    this.teammates$ = this.searchTerms.pipe(
      // wait 300ms after each keystroke before considering the term
      debounceTime(300),
 
      // ignore new term if same as previous term
      distinctUntilChanged(),
 
      // switch to new search observable each time the term changes
      switchMap((term: string) => this.restApiService.searchTeammates(term)),
    );
  }

  // Push a search term into the observable stream
  search(term: string): void {
    // console.log(this.searchTerms);
    this.searchTerms.next(term);
  }
}

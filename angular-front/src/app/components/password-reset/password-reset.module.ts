import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { PasswordResetRoutingModule } from './password-reset-routing.module';
//import { PasswordResetComponent } from './password-reset.component';

@NgModule({
  imports: [
    CommonModule,
    PasswordResetRoutingModule,
    FormsModule
  ],
  declarations: [
    //PasswordResetComponent
  ]
})
export class PasswordResetModule { }

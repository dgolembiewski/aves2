import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder, Validators, AbstractControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RestApiService } from '../../services/restapi.service';

@Component({
  selector: 'app-password-reset',
  templateUrl: './password-reset.component.html',
  styleUrls: ['./password-reset.component.css']
})
export class PasswordResetComponent implements OnInit {

  responseMessage: string;
  token: string;
  newPassword: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private restApiService: RestApiService) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get('token');
  }

  onSubmit(data): void {

    if(data.newPassword != data.passwordConfirm)
    {
      this.responseMessage = "Passwords do not match!";
    }
    else
    {
      this.restApiService.updatePassword(this.token, data.newPassword)
        .subscribe(res => {
          alert(this.responseMessage = res.message);
          this.router.navigate(['/login']);
        },
        err => {
          console.log(err);
          this.responseMessage = err.error.message;
        });
    }
  }
}

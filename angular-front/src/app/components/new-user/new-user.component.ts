import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
// import { NewUserService } from '../../services/new-user.service';
import { RestApiService } from '../../services/restapi.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

// model: any = {};
registrationMessage: string;

constructor(private restApiService: RestApiService, private router: Router) {}

  ngOnInit() {}

  @ViewChild(NgForm) myForm: NgForm;

  onSubmit(userData): void {

    this.restApiService.postUser(userData.firstName, userData.lastName,userData.username,
                                 userData.email,userData.password)
                                 .subscribe(
                                  res  => {
                                  this.registrationMessage = '';
                                  console.log('New user added ' + userData.firstName);
                                  this.router.navigate(['/login']);
                                  //this.myForm.resetForm();
                                 },
                                 err => {
                                  console.log(err);
                                  this.registrationMessage = 'User already exists';
                                  this.myForm.resetForm();
                                 });
  }
}


export class Teammate {
    id: string; // corresponds with src_id from MemberComponent
    firstName: string;
    fname: string;
    lastName: string;
    lname: string;
    nickName: string;
    bio: string;
    email: string;
    phone: string;
    location: string;
    position: string;
    positionId: number;
    company_name: number; // corresponds with id from Company
    companyId: number;
    deleted: boolean;
    photo: string;
    editable: boolean;
    freeAgent: boolean;
    availableDate: Date;
    skills: Array<{}>;
    team: number;

    constructor() {
        this.id = '';
        this.fname = '';
        this.lname = '';
        this.nickName = '';
        this.bio = '';
        this.email = '';
        this.phone = '';
        this.location = '';
        this.position = '';
        this.position = null;
        this.companyId = null;
        this.deleted = false;
        this.photo = 'https://www.w3schools.com/howto/img_avatar.png';
        this.editable = false;
        this.freeAgent = true;
        this.availableDate = null;
        this.skills = null;
        this.team = null;
    }
}

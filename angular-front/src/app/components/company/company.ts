export class Company {
    id: number; // corresponds with companyId from Teammate
    companyName: string;
    deleted: boolean;

    constructor() {
        this.id = null;
        this.companyName = '';
        this.deleted = false;
    }
}
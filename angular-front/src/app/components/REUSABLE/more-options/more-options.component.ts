import { Component, OnInit, Input  } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-more-options',
  templateUrl: './more-options.component.html',
  styleUrls: ['./more-options.component.css']
})
export class MoreOptionsComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private location: Location,
              public router: Router) { }

  @Input() options : string[]
  @Input() teamMember : any

  ngOnInit() {
  }

  tableOptions(selector) : void {
      if (selector == 'Edit') {
        this.router.navigate([`memberdetail/${this.teamMember.id}`])
      }
      else if (selector == 'Delete') {
        console.log('Deleted')
      }
  }

}

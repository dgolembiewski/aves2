export class Team {
    id: number; // takes on {1,2,3,4,5,6,7,8,9,10}
    name: string;
    parentTeam: number; // takes on {1,5,8,null}
    deleted: boolean;

    constructor() {
        this.id = null;
        this.name = '';
        this.parentTeam = null;
        this.deleted = false;
    }
}
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MembersComponent, ModalWindow } from './members.component';
import { MemberDetailComponent } from '../member-detail/member-detail.component';

const routes: Routes = [
  {
    path: '',
    component: MembersComponent
  },
  {
    path: 'memberdetail/:id',
    component: MemberDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MembersRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MembersComponent, ModalWindow } from './members.component';
import { MemberDetailComponent } from '../member-detail/member-detail.component';
import { MemberSearchModule } from '../member-search/member-search.module';
import { MoreOptionsComponent } from '../REUSABLE/more-options/more-options.component';
import { MatDialogModule } from '@angular/material';
import { TooltipComponent } from '../tooltip/tooltip.component';
import { MembersRoutingModule } from './members-routing.module';

import { NgForm, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';

import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    MembersRoutingModule,
    MemberSearchModule,
    MatDialogModule,
    FormsModule,
    FileUploadModule,
    InfiniteScrollModule

  ],
  declarations: [
    MembersComponent,
    MemberDetailComponent,
    ModalWindow,
    MoreOptionsComponent,
    TooltipComponent
  ],
  entryComponents: [
    ModalWindow
  ]
})
export class MembersModule { }

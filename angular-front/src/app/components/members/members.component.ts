import { Component, OnInit, Input, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

// import { Teammate } from '../teammate/teammate';
// import { Company } from '../company/company';
// import { Member } from '../members/member/member';
import {Location} from '../../shared/models/location.model';
import {Company} from '../../shared/models/company.model';
import {Position} from '../../shared/models/position.model';
import {Teammate} from '../../shared/models/teammate.model';
import {Member} from '../../shared/models/member.model';

// import { TeammateService } from '../../services/teammate.service';
// import { CompanyService } from '../../services/company.service';
// import { MemberService } from '../../services/member.service';
import { RestApiService } from '../../services/restapi.service';


@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})

export class MembersComponent implements OnInit {

  teammates: Teammate[];
  companies: Company[];
  members: Member[];
  combined = [];
  allMembers = [];
  displayFreeAgent : boolean;
  view = 'grid';
  name = '';
  email = '';
  today = new Date();
  section = [3, 6];
  teammate: Teammate;
  finishedCalls = [false, false, false];
  throttle = 300;
  scrollDistance = 1;
  scrollUpDistance = 2;
  direction = '';
  modalOpen = false;
  timeout = 0;
  callMore = true;
  previousPos = 0;

  modalWindowDialogRef: MatDialogRef<ModalWindow>;

  constructor(
              private restApiService: RestApiService,
              public router: Router,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.displayFreeAgent = false;

    //When all the callbacks get called, the finishedCalls array will be all true
    this.restApiService.updateTeammatesArray(() => { this.displayMembers(0) } );
    this.restApiService.updateCompaniesArray(() => { this.displayMembers(1) } );
    this.restApiService.updateMembersArray(() => { this.displayMembers(2) } );

    var that = this;

    //Wheel is required for better user experience for users who are zoomed out of the page
    //These users do not have scroll access yet
    window.addEventListener('wheel', (e) => {
      var currentPos = document.body.scrollTop;
      var maxPos = document.body.scrollHeight;
      var relativePos = document.body.clientHeight; //Used to accomodate for zoom
      var relativityFactor = 1.2; //Used to add space to relativePos to trigger call before absolute bottom
      var fromBottom = maxPos - currentPos;
      var delta;

      if (e.wheelDelta) {
          delta = e.wheelDelta;
      } else{
          delta = -1 * e.deltaY;
      }
      //Wheel Down
      if (delta < 0) {
        //Call data only if there is no scrollbar
        if (maxPos == relativePos && currentPos == 0) {
          //console.log('SCROLLBAR DOES NOT EXIST, USING WHEEL');
          //Call more data only when close to the bottom of the page
          if (fromBottom <= relativePos * relativityFactor) {
            if (that.timeout <= e.timeStamp) {
              that.timeout = e.timeStamp + 500;
              if (that.callMore) {
                that.onScrollDown();
              } else {
                window.removeEventListener('wheel', (e) => {}, true);
              }
            }
          }
        } else {
          //console.log('SCROLLBAR EXISTS, NOT USING WHEEL');
        }
      } else if (delta > 0) {
        //Wheel Up
        this.onScrollUp();
      }
    }, true);
    
    //Scroll takes care if user scrolls to bottom of page
    //Scroll up/down not necessary
    window.addEventListener('scroll', (e) => {
      var currentPos = document.body.scrollTop;
      var maxPos = document.body.scrollHeight;
      var fromBottom = maxPos - currentPos;
      var relativePos = document.body.clientHeight; //Used to accomodate for zoom
      var relativityFactor = 1.2; //Used to add space to relativePos to trigger call before absolute bottom
      var fromBottom = maxPos - currentPos;
      //Compare directions by pixel change
      //Use if you need specific scroll up/down events
      if (currentPos > that.previousPos) {
        if (that.direction != 'Down') {
          that.direction = 'Down';
        }
      } else {
        if (that.direction != 'Up') {
          that.direction = 'Up';
        }
      }
      that.previousPos = currentPos;
      //Call more data only when close to the bottom of the page, scroll direction does not matter here
      if (fromBottom <= relativePos * relativityFactor) {
        if (that.timeout <= e.timeStamp) {
          that.timeout = e.timeStamp + 500;
          if (that.callMore) {
            //console.log('Reached Bottom so scrolled!');
            that.onScrollDown();
          } else {
            window.removeEventListener('scroll', (e) => {}, true);
          }
        }
      }
    }, true);
  }
  
  onScrollDown () {
    console.log('Scrolled down to bottom. Calling more data!');
    this.section[0] += 3;
    this.section[1] += 3;
    this.getTeammates(this.section);
  }
  
  onScrollUp() {
    console.log('Scrolled up!');
  }

  displayMembers(callNumber: number): void {
    this.finishedCalls[callNumber] = true;
    if (this.finishedCalls[0] && this.finishedCalls[1] && this.finishedCalls[2]) {
      //Add functionality to wait to grab the next 3 if the first 3 have not finished being called yet
      this.getTeammates([0, 6]); //First run should grab 6, then rest grab 3 each time
    }
  }

  getTeammates(section: number[]): void {
    //Start queuing these up so that you cannot call another section until the first call is returned
    this.teammates = this.restApiService.getTeammatesBySection(section[0], section[1]);
    if (this.teammates.length < 1) {
      this.callMore = false;
      console.log('Out of teammates to call!');
    } else {
      //console.log('Not out of teammates to call.');
      this.getCompanies(section);
    }
  }

  getCompanies(section: number[]): void {
    this.restApiService.getCompanies()
    .subscribe(companies => {
      this.companies = companies;
      this.getMembers(section);
    });
  }

  getMembers(section: number[]): void {
    this.restApiService.getMembers()
    .subscribe(members => {
      this.members = members;
      this.combineData();
      this.allMembers = this.combined;
    });
  }

  combineData(): void {

    this.teammates.forEach(teammate => this.combined.push(teammate));

    // Handle OrangeMod2 changes
    /* this.combined.forEach(combinee => {
      combinee.firstName = combinee.fname;
      combinee.lastName = combinee.lname;
      combinee.company_name = combinee.companyId;
    }); */

    // map company names from Company to company ID in Teammate
    /*this.combined.forEach(combinee => {

      const matchIdxComp = this.companies.findIndex(company => {
        return company.id === combinee.companyId;
      });

      if (matchIdxComp >= 0) {
        combinee.companyName = this.companies[matchIdxComp].companyName;
      } else if (matchIdxComp === -1) {
        combinee.companyName = 'no match found';
      }
    }); */

    this.combined.forEach(combinee => {

        const matchIdx = this.members.findIndex(member => {
          return member.src_id === combinee.id;
        });

        if (matchIdx >= 0) {
          combinee.skills = this.members[matchIdx].skills.sort( function (obj1, obj2) {
            return obj2.rating - obj1.rating;
          });
          combinee.bio = this.members[matchIdx].bio;
          combinee.photo = this.members[matchIdx].photo;
        } else if (matchIdx === -1) {
          combinee.skills = [{title: 'no match found', rating: 0}];
          combinee.bio = 'no match found';
        }

    });
  }

  toggleList(): void {
    this.view = 'list';
  }

  toggleGrid(): void {
    this.view = 'grid';
  }


  // add user
  public add_user() {
    // const new_employee = new Teammate();
    // this.combined.unshift(new_employee);

    this.router.navigate([`/adduser`]);

  }

  // free_agent_only
  public free_agent_only() {    
    this.combined = this.combined.filter(combined => combined.isFreeAgent === true);
    this.displayFreeAgent = true;
  }
  
// all members
public all_members() {
  this.combined = this.allMembers;
  this.displayFreeAgent = false;   
}

  // Flip the card and display the back
  public flip_card(teammate) {
    document.getElementById('flip-user-' + teammate.id).classList.toggle('flip');
  }

  public delete_user(teammate) {
    if (confirm('Are you sure you want to delete ' + teammate.fname + ' ' + teammate.lname + '?')) {
      this.restApiService.deleteTeammate(teammate.id).subscribe();
      teammate.deleted = true;
      this.combined = this.combined.filter(member => member !== teammate);
      alert('User has been deleted!');
    }
  }


  openModalWindowDialog(person?) {
    this.modalWindowDialogRef = this.dialog.open(ModalWindow, {
      data: {
        employeeId: person ? person.id : '',
        employeeFirstName: person ? person.firstName : '',
        employeeLastName: person ? person.lastName : '',
        employeeEmail: person ? person.email : '',
        employeePhone: person ? person.phone : '',
        employeeLocation: person ? person.location : '',
        employeePosition: person ? person.position : '',
        employeeCompany: person ? person.companyName : '',
        // employeeTeam: person ? person.team : '',
        // employeeSubteam: person ? person.subteam : ''
      },
      height: '300px',
      width: '300px'

    });

  }

}

@Component({
  selector: 'app-modal-window',
  templateUrl: './modal-window.html',
  styleUrls: ['./modal-window.css']
})
export class ModalWindow implements OnInit {
  employeeId: any;
  employeeFirstName: any;
  employeeLastName: any;
  employeeEmail: any;
  employeePhone: any;
  employeeLocation: any;
  employeePosition: any;
  employeeCompany: any;
  // employeeTeam: any;
  // employeeSubteam: any;

  constructor(
    private dialogRef: MatDialogRef<ModalWindow>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit() {

    this.employeeId = this.data.employeeId;
    this.employeeFirstName = this.data.employeeFirstName;
    this.employeeLastName = this.data.employeeLastName;
    this.employeeEmail = this.data.employeeEmail;
    this.employeePhone = this.data.employeePhone;
    this.employeeLocation = this.data.employeeLocation;
    this.employeePosition = this.data.employeePosition;
    this.employeeCompany = this.data.employeeCompany;
    // this.employeeTeam = this.data.employeeTeam;
    // this.employeeSubteam = this.data.employeeSubteam;

  }



}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AddUserRoutingModule } from './add-user-routing.module';
import { AddUserComponent } from './add-user.component';


@NgModule({
  imports: [
    CommonModule,
    AddUserRoutingModule,
    FormsModule
  ],
  declarations: [
    AddUserComponent
  ]
})
export class AddUserModule { }

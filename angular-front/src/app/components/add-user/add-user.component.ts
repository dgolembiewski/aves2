///// IMPORT DEPENEDENCIES /////
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

///// IMPORT SCHEMA COMPONENT /////
// import { Teammate } from '../teammate/teammate';
// import { Company } from '../company/company';
// import { Member } from '../members/member/member';
import {Company} from '../../shared/models/company.model';
import {Teammate} from '../../shared/models/teammate.model';
import {Member} from '../../shared/models/member.model';

///// IMPORT SERVICES /////
import { RestApiService } from '../../services/restapi.service';
//import { MemberService} from '../../services/member.service';


///// COMPONENT DECORATORS /////
@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})


///// COMPONENT CLASS /////
export class AddUserComponent implements OnInit {

  ///// DEFINE PROPERTIES /////
  teammate:   Teammate;
  company:    Company;
  member:     Member;
  companies:  Company[];

  ///// CONSTRUCTOR /////
  constructor(private route: ActivatedRoute,
              // private teammateService: TeammateService,
              // private companyService: CompanyService,
              // private memberService: MemberService,
              private restApiService: RestApiService,
              private location: Location) { }

  ///// SET INITIAL PROPERTIES /////
  ngOnInit() {
    // Create new Teammate object
    this.teammate = new Teammate();

    // Create new Member object
    this.member = new Member();
    this.member.skills = [];

    // Get companies
    this.restApiService.getCompanies().subscribe(companies => {
      this.companies = companies;
    });
  }

  ///// GO BACK BUTTON /////
  goBack(): void {
    this.location.back();
  }

  ///// ADD-REMOVE SKILLS /////
  addSkill(): void {
    this.member.skills.push({});
  }
  removeSkill(indx): void {
    this.member.skills.splice(indx, 1);
  }

  ///// CREATE USER BUTTON /////
  addUser(): void {
    this.addNewTeammate();
  }

  ///// ADD TEAMMATE TO PG DB /////
  addNewTeammate(): void {
    this.restApiService.addNewTeammate(this.teammate)
      .subscribe(teammate => {
        this.teammate = teammate;
        this.addNewMember();
      });
  }

  ///// ADD MEMBER TO MG DB /////
  addNewMember(): void {
    this.member.src_id = this.teammate.id;
    this.restApiService.addNewMember(this.member).subscribe( () => this.location.back());
  }
}

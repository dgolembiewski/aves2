import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../../app.component';
import { Router } from '@angular/router';
import { RestApiService } from '../../services/restapi.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginMessage: string;
  first_name: string;
  last_name: string;

  constructor(
    public appComponent:AppComponent, 
    private router:Router, 
    private loginService:RestApiService) { }

  ngOnInit() {
  }

  // retrieve login credentials from form and check if user is in the database
  onClickSubmit(data): void {
    this.loginService.getLogin(data.username, data.password).subscribe(res => {
      console.log(res)
      localStorage.setItem('id_token', res.accessToken);
      this.loginMessage = '';
      localStorage.setItem('user', 'true');
      this.router.navigate(['members']);
    },
    err => {
      console.log(err);
      this.loginMessage = 'Login failed!';
    });
  }

  // getName(data):void {
  //   this.loginService.getLogin(data.username, data.password, data.first_name).subscribe( res => {
  //     this.first_name = data.first_name
  //   })
  // }
}

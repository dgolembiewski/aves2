import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CommunicationsRoutingModule } from './communications-routing.module';
import { CommunicationsComponent } from './communications.component';

@NgModule({
  imports: [
    CommonModule,
    CommunicationsRoutingModule
  ],
  declarations: [
    CommunicationsComponent
  ]
})
export class CommunicationsModule { }

import { CommunicationsModule } from './communications.module';

describe('CommunicationsModule', () => {
  let communicationsModule: CommunicationsModule;

  beforeEach(() => {
    communicationsModule = new CommunicationsModule();
  });

  it('should create an instance', () => {
    expect(communicationsModule).toBeTruthy();
  });
});

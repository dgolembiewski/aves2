import { Component, OnInit } from '@angular/core'; // modules and dependancies
import { Task } from './task/task'; //import components
// import { TaskService } from '../../services/task.service';  // import services
import { environment } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Teammate } from '../teammate/teammate';
// import { Task } from '../task/task.component';
import { RestApiService } from '../../services/restapi.service';

//Component Meta Data
@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})


export class TasksComponent implements OnInit {
  tmExample: Teammate;
  tasks : Task[]; //an array of Tasks

  // selectedTask: Task;
  public url = environment.nodeApiUrl +'/apijava';
  public httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };


  constructor(
  //   private taskService: TaskService,
      private http: HttpClient,
      private restApiService: RestApiService) { }

  ngOnInit() {
    this.getTasks();
  }

  getTasks(): void {
    this.restApiService.getTasks()
      .subscribe(tasks => this.tasks = tasks);
  }

  testJavaApi():any{

    var ar = {
      'users':        '/users',
      'users:/id':    '/users/2b726fc3-8d3f-41eb-bb11-2eeb384b36c9',
      'teams':        '/teams',
      'teams:/id':    '/teams/10',
      'locations':    '/locations',
      'locations/id': '/locations/1',
      'companies':    '/companies',
      'companies/id': '/companies/1',
      'positions':    '/positions',
      'positions/id': '/positions/1',
      'skills':       '/skills',
      'userSearch':   '/userSearch?name=ely',
      'skills/:name': '/skills/Java'
    }

    Object.entries(ar).forEach(urlTest =>{
      this.http.get(this.url+urlTest[1]).subscribe(data =>{
        console.log(this.url+urlTest[1],': ',data);
        console.log('\n');
      });
    });

  //   //Update USER
  // this.http.get<Teammate>(this.url+ '/users/2b726fc3-8d3f-41eb-bb11-2eeb384b36c9').
  //   subscribe(u =>{
  //     this.tmExample = u;
  //     this.tmExample.nickName= 'THIS IS A TEST1234';
  //     this.tmExample.bio = 'Software engineer working on Web Application program!!????'
  //     // console.log('Before: ',this.tmExample);
  //     this.http.put(this.url+ '/users/2b726fc3-8d3f-41eb-bb11-2eeb384b36c9',u).
  //     subscribe(data =>{
  //       console.log('Returned user from update: ',data);
  //     });
  //   });

  // //Create User and Delete User
  // WARNING:: THIS WILL CREATE AN ITEM IN BOTH postgres AND MONGO... 
  // WARNING:: Be very Careful how many times you run this, 
  // WARNING:: neither row actually get deleted (just marked deleted)
    // this.tmExample = new Teammate;

    // this.tmExample.firstName = 'Bobs';
    // this.tmExample.lastName = 'Your Uncle';
    // this.tmExample.nickName = 'really?';
    // this.tmExample.bio = "Why did you choose that of all things";
    // this.tmExample.email = 'umm@gmailNotReally.com';
    // this.tmExample.phone ='123456789';
    // this.tmExample.location = null;
    // this.tmExample.positionId = 3 ;
    // this.tmExample.companyId = 3;
    // this.tmExample.deleted = false;
    // this.tmExample.editable = true;
    // this.tmExample.isFreeAgent = false;
    // this.tmExample.availableDate = new Date('08/25/2018');
    // this.tmExample.skills = [{'title': 'Java', 'rating':4}];
    // this.tmExample.team = 3;
    // this.http.post(this.url+'/users', this.tmExample).subscribe( (u) =>{
    //   // if err you have a probably have a dup email addr.
    //   console.log('Created id = ', u);
    //   this.http.delete(this.url+'/users/'+u).subscribe(x=>{console.log(x);})
    // });








  }






  // onSelect(task: Task): void {
  //   this.selectedTask = task;
  // }

}

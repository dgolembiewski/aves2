export class Task {
  //declare properties with types
  id: number; //unique id number for the task
  title: string;
  assigned: string; //date that that the task is assigned
  due: string; //due date for task
  assigner: string; // who the task is assigned to ... will eventually tie to clients 
  assignee: string; // who the task is assigned to ... will eventually tie to clients 
  description: string;

  constructor() { }

}

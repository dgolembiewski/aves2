var replace = require('replace-in-file');

//arg 0 is node process itself
//arg 1 is this file name

var argLen = process.argv.length;
//console.log('argLen=' + argLen);

if (argLen < 6)  {
	console.log('must specify environment newVersion newDate newBy arguments');
	return;
}

// "falcon-ci" "2018.0612.1147.903" "Tue" "06/12/2018" "jenkinsci"
// 2 = env = falcon-ci
// 3 = v = 2018.0612.1147.903
// 4 = bd = "Tue"
// 5 = full date = "06/12/2018"
// 6 = built by = jenkinsci

var env = process.argv[2];
var newVersion = process.argv[3];
var newDate = process.argv[5];
var newBy = process.argv[6];

var envFile = 'src/environments/environment.' + env +'.ts';
console.log('prebuildUpdate envFile found at ' + envFile);

//for (let j = 0; j < process.argv.length; j++) {  
//   console.log(j + ' -> ' + (process.argv[j]));
//}

const versionOptions = {
    files: envFile,
	from: /version: '(.*)'/g,
	to: "version: '" + newVersion + "'"
};

const buildDateOptions = {
    files: envFile,
	from: /buildDate: '(.*)'/g,
	to: "buildDate: '" + newDate + "'"
};
 
const builtByOptions = {
    files: envFile,
	from: /builtBy: '(.*)'/g,
	to: "builtBy: '" + newBy + "'"
};
 
try {
    console.log('Setting build attribs: v=' + newVersion + '; bd=' + newDate + '; bb=' + newBy);

    let changedFiles = replace.sync(versionOptions);	
	let buildDateFiles = replace.sync(buildDateOptions);
	let builtByFiles = replace.sync(builtByOptions);
		
    console.log('Build attribs set: v=' + newVersion + '; bd=' + newDate + '; bb=' + newBy);
}
catch (error) {
    console.error('Error occurred:', error);
    //throw error
}
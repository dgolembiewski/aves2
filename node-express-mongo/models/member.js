
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const memberSchema = new Schema({
    src_id: String,
    photo: String,
    bio: String,
    skills: [
      {
        title: String,
        rating: String
      }
    
    ],
    photo: String
});

module.exports = mongoose.model('project_member', memberSchema);

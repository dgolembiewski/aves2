'use strict';


// Teamate -> PG database relation
let user = { 
    id: 'id',
    firstName : 'first_name',
    lastName : 'last_name',
    nickName : 'nick_name',
    email : 'email',
    phone : 'phone',
    team : 'subteam_id',
    location : 'location_id',
    companyId : 'company_id',
    positionId : 'position_id',
    isFreeAgent : 'is_free_agent',
    availableDate : 'available_date',
    isDeleted : 'is_deleted',
    created_by : 'created_by',
    created_ts: 'created_ts',
    modified_by : 'modified_by',
    modified_ts : 'modified_ts'

}

module.exports = Object.freeze(user);
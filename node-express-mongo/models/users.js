'use strict';
const async = require('async');
const pgUser = require('./postgresUser');


// This class is a helper class for the User Object...
// It is called from apijava and will make the postgres and mongo calls
// Combine/Seperate the data as needed and Present to the api a unified User object

// All the fucntions use callbacks to return their data.
// We use async to make multiple different db calls at the same time in order to speed up responses -- see getUsers for a simple example
class Users{

    constructor( pg, mg){
        this.postgres = pg;
        this.mongo = mg;
    }

    getAllUsers(cb){ // This helper function runs very similarly to getUsers -- look at that for full comments
        this.combineUsers(null, cb); // This function is used here and in userSearch (1st arg gives a search criteria, since we want all users here leave it null)
    }

    // Gets a single User from both db's exptects an id from pg
    getUsers(id, cb){
        var dataP, dataM;
        var that = this;
        async.parallel([
                (callback)=>{this.postgres.getUsers(id, callback);}, 
                (callback)=>{this.mongo.getSingleUser(id, callback);}
        ], function (err, r) { // this is called once both above functions have called 'callback'
            if(err){ 
                return cb(err, null); // we return here so we dont callback to apijava multiple times if there is an error
            }
            else{
                dataP = r[0]; // this is from the 1st function
                dataM = r[1]; // this is from the 2nd function
                var x = dataP.rows[0]; // make our user object base equal to the post gres data
                Object.entries(dataM._doc).forEach(item =>{ // go through every item in the Mongo call and add it to our user object
                    if (item[0] != 'src_id' && item[0] != '_id'){
                        x[item[0]] = item[1];
                    }
                });

                // Calls for all the sub objects
                async.parallel([ 
                    (callback)=>{that.postgres.getSingleTeamName(x.subteam_id, callback);},
                    (callback)=>{that.postgres.getCompanies(x.company_id, callback);},
                    (callback)=>{that.postgres.getLocations(x.location_id, callback);},
                    (callback)=>{that.postgres.getPositions(x.position_id, callback);},
                    (callback)=>{that.postgres.getUserRoles(x.user_id, callback);},
                    (callback)=>{that.postgres.getUserActivations(x.user_id, callback);},
                ], function(err, data){ // all calls have finished (or one has errored)
                    if(err){
                        return cb(err, x);
                    }
                    // If these calls return actual data -- add them to our User, otherwise add a empty array
                    x['teams'] = (data[0].rowCount > 0) ? data[0].rows[0] : [];
                    x['company'] = (data[1].rowCount > 0) ? data[1].rows[0] : [];
                    x['location'] = (data[2].rowCount > 0) ? data[2].rows[0] : [];
                    x['position'] = (data[3].rowCount > 0) ? data[3].rows[0] : [];
                    x['userRoles'] = (data[4].rowCount > 0) ? data[4].rows[0] : [];
                    x['userActivations'] = (data[5].rowCount > 0) ? data[5].rows[0] : [];
                    x = that.modifyReturn(x); // helper function we use to match naming conventons with the java api
                    cb(null, x); // return the data
                });
            }
        });
    }
    
    // Updates a User takes an id and a user object
    updateUsers(id, body, cb){
        var b= this.splitUser(body); // splits the user into postgres data and mongo data based on the mongo schema
        var bPostgres = b[0];
        var bMongo = b[1];

        async.parallel([
            (callback)=>{this.postgres.updateUsers(id, bPostgres, callback);}, 
            (callback)=>{this.mongo.updateUsers(id, bMongo, callback);}
        ], function (err, r) {
            if(err){
                return cb(err, null);
            }
            else{
                return cb(null, r);
            }
        });
    }

    // Creates a New user -- expects a User object
    insertUsers(body, cb){
        var that = this;
        var b= this.splitUser(body);
        var bPostgres = b[0];
        var bMongo = b[1];
        // We seperate the 2 calls here because we need the new id from postgres before we can update mongo
        this.postgres.insertUsers(bPostgres, (err, result)=>{
            if(err){
                return cb(err, null);
            }
            var id = result.rows[0].id;
            // We use the same call that we did in updateUsers -- it works because if mongo cannot find that item, it creates it
            that.mongo.updateUsers(id, bMongo, (err, result)=>{
                if(err){
                    return cb(err, result.rows[0]);
                }
                return cb(null, id);
            });
        });
    }
    

    // Search Function expects part of a first, last, or nickname
    getUserSearch(name, cb){
        this.combineUsers(name, cb);
    }

    // Returns takes a skill, gets all the src_id's from mongo with that skill
    // Calls the postgres database for all those users
    // returns a sorted list based on the rating of that skill
    // Doesnt return full user object.. just a partial
    getSkill(skillName , cb){
        if (!skillName){
            return cb("No Skill Name Given", null);
        }
        var that = this;
        var dataP, dataM, teams;
        this.mongo.getUserBySkill(skillName, (err, data)=>{
            if(err){
                return cb(err)
            }
            // get src id to call postgres
            var srcList = []
            data.forEach(item =>{
                srcList.push(item.src_id);
            });
            dataM = data;

            async.parallel([
                (callback)=>{this.postgres.getUsersById(srcList, callback);}, 
                (callback)=>{this.postgres.getTeamNames(callback);},
                ], function (err, r) {
                if(err){ 
                    return cb(err, null);
                    
                }
                else{
                    dataP = r[0].rows;
                    teams = r[1];
                    var finalList = [];
                    dataM.forEach(element => {
                        var x = element.toJSON(); // required to turn Mongo object into json

                        // Match our mongo and postgres data sets based on id/src_id
                        var q = dataP.find( function(obj) {return obj.id === element.src_id;}); 
                        if (q){ //Matching Id in Postgres
                            
                            var teamList = teams.rows.find( function(obj){return obj.Subteam_Id === q.subteam_id;});
                            q['teams'] = teamList;
                            Object.entries(q).forEach(item =>{     
                                x[item[0]] = item[1];
                                
                            });
                            x = that.modifyReturn(x);
                            finalList.push(x);
                        }
                        else{ // There not a matching Id is postgres.
                            // console.log('No id for: ', x);
                        }

                    });
                    finalList = that.sortList(finalList, skillName); // sort our list based on skill raiting
                    cb(null, finalList);
                }
            });
        });
        
    }

    // see getUser for full explanition
    combineUsers(user, cb){
        var that = this;
        var dataP, dataM, teams, companies, locations, positions, roles, activation;
        async.parallel([
                (callback)=>{(user) ? this.postgres.getUsersSearch(user, callback) : this.postgres.getAllUsers(callback);}, 
                (callback)=>{this.mongo.getUsers(callback);},
                (callback)=>{this.postgres.getTeamNames(callback);},
                (callback)=>{this.postgres.getAllCompanies(callback);},
                (callback)=>{this.postgres.getAllLocations(callback);},
                (callback)=>{this.postgres.getAllPositions(callback);},
                (callback)=>{this.postgres.getAllUserRoles(callback);},
                (callback)=>{this.postgres.getAllUserActivations(callback);}
        ], function (err, r) {
            if(err){ 
                return cb(err, null);
            }
            else{
                dataP = r[0];
                dataM = r[1];
                teams = r[2];
                companies = r[3];
                locations = r[4];
                positions = r[5];
                roles = r[6];
                activation = r[7];
                var finalList = [];
                dataP.rows.forEach(element => {
                    var x = element;
                    var teamList = teams.rows.find( function(obj){return obj.Subteam_Id === x.subteam_id;});
                    var companyList = companies.rows.find(function(obj){return obj.id === x.company_id; });
                    var locationList = locations.rows.find(function(obj){return obj.id === x.location_.id; });
                    var positionsList = positions.rows.find(function(obj){return obj.id === x.position_id; });
                    var rolesList = roles.rows.find(function(obj){return obj.user_id === x.id; });
                    var activationList = activation.rows.find(function(obj){return obj.user_id === x.id; });

                    x['teams'] = teamList ? teamList : [];
                    x['company'] = companyList? companyList : [];
                    x['location'] = locationList? locationList : [];
                    x['position'] = positionsList? positionsList : [];
                    x['userRoles'] = (rolesList) ? rolesList : [];
                    x['userActivations'] = activationList? activationList : [];

                    var q = dataM.find( function(obj) {return obj.src_id === element.id;});
                    if (q){ //Matching Id in Mongo

                        Object.entries(q._doc).forEach(item =>{
                            if (item[0] != 'src_id' && item[0] != '_id'){
                                x[item[0]] = item[1];
                            }
                        });
                    }
                    x = that.modifyReturn(x);
                    finalList.push(x);
                });
                cb(null, finalList);
            }
        });
        
    }
    
    // Splits a user obj based on mongo schema
    splitUser(body) {
        var b1 ={}, b2 ={};
        var z = Object.keys(this.mongo.project_member.schema.obj)
        Object.entries(body).forEach(item=>{
            if (z.includes(item[0])){ // Update Mongo
                b2[item[0]] = item[1];
            }
            else{ // Update Postgres
                b1[item[0]] = item[1];
            }
        });
        return ([b1, b2]);
    }

    // Renames our Keys to Match Front End Expectations
    modifyReturn(userObj){
        Object.entries(pgUser).forEach(element =>{
            if (userObj.hasOwnProperty(element[1]) && userObj[element[0]] != userObj[element[1]]){
                userObj[element[0]] = userObj[element[1]]
                delete userObj[element[1]];
            }
        });
        return userObj;
    }

    // Sort based on rating of given skill
    sortList(li, skillName){
        li.sort(function(a,b){
            var aR, bR = 0;
            for (var i = 0 ; i< a.skills.length; i++){
                if (a.skills[i].title === skillName){
                    aR = a.skills[i].rating;
                    break;
                }
            }
            for (var i = 0 ; i< b.skills.length; i++){
                if (b.skills[i].title === skillName){
                    bR = b.skills[i].rating;
                    break;
                }
            }
            return aR < bR;

         });
        return li;
    }
    
}

module.exports = Users;
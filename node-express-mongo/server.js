"use strict";

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const app = express();
const cors = require('cors');
const config = require('./cfg/config.js');

const api = require('./routes/api');
const apijava = require('./routes/apijava');

const swaggerUi = require('swagger-ui-express');
const YAML = require('yamljs');
const swaggerDocument = YAML.load('./swagger/swagger.yaml');

var options = {
  explorer : false,
  swaggerOptions: {
      validatorUrl : null
    }
};

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));

app.use(cors())

// Parsers
app.use(bodyParser.json({limit:1024*1024*20}));
app.use(bodyParser.urlencoded({ extended: false, limit: '100mb', parameterLimit: 100000000 }));

// Serve static files
app.use(express.static(path.join(__dirname, 'dist')));

// Setup API routes
app.use('/api', api);
app.use('/apijava', apijava);

// Return other routes to Angular index file
// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'dist/index.html'));
// });

//set app port
const port = config.PORT;//process.env.PORT || '3000';
app.set('port', port);

// Create the HTTP server
const server = http.createServer(app);

server.listen(port, () => {
    console.log(`Running on localhost:${port}`)
    console.log('pid is ' + process.pid);
});

process.on('SIGTERM', function () {
    server.close(function () {
        console.log('SIGTERM');
        process.exit(0);
    });
});

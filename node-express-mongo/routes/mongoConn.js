"use strict";

const mongoose = require('mongoose');
const config = require('../cfg/config.js');



// Required For File Upload
var formidable = require('formidable');
var Grid = require('gridfs-stream')
var fs = require('fs')
Grid.mongo = mongoose.mongo;


// Basic Mongo calls using mongoose
class MongoConn {
    constructor(){
        const db = config.MONGODB_URL;
        mongoose.Promise = global.Promise;
        mongoose.connect(db, function (err) {
        if (err) {
            console.log('\n\tMongoDB connection error!!!\n');
        }
        });
        this.project_member = require('../models/member');
    };

    getUsers(cb){
        this.project_member.find({}, (err, data) =>{
            if(err) {
                console.log("Mongo Error: ", err);
                return cb(err, null);
            }
            else{
                cb(null, data);
            }
        });
    };

    getSingleUser(id, cb){
        this.project_member.findOne({src_id: id} , (err, data)=>{
            if(err) {
                console.log("Mongo Error: ", err);
                return cb(err, null);
            }
            else{
                cb(null, data);
            }
        });
    }

    getSkills(cb){
        this.project_member.aggregate([{$unwind:"$skills"},{$match:{"skills.title":{$ne:null}}},{$group:{_id:"$skills.title",total:{$sum:1}}},
        { $sort: { total: -1 } }], 
        (err, data)=>{
            if(err) {
                console.log("Mongo Error: ", err);
                return cb(err, null);
            }
            else{
                cb(null, data);
            }
        });

    }

    getUserBySkill(skillName, cb){

        this.project_member.find({skills:{$elemMatch:{ title: skillName}} }, (err, data)=>{
            if(err) {
                return cb(err, null);
            }
            else{
                cb(null, data);
            }
        });

    }
    updateUsers(id, body, cb){
        this.project_member.findOneAndUpdate({src_id: id}, body, {upsert:true}, (err, data)=>{
            if(err) {
                console.log('Update Error');
                return cb('Mongo: '+ err, null);
            }
            cb(null, data);
        });
    }
}

module.exports = MongoConn;
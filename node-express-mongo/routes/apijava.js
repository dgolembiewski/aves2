'use strict';


const express = require('express');
const router = express.Router();

var postgresConn = require('./postgresConn'); 
var mongoConn = require('./mongoConn'); 
var postgres = new postgresConn();  // The Postgres Controller
var mongo = new mongoConn();        // The mongo Contoller

// Combine the 2 db's
// Using a differnt file for users since it has more complex logic
var users = require('../models/users'); 
var user = new users(postgres, mongo); 

// Get All Users
// Returns [User, User, User...]
router.get('/users',(req, res) =>{
    user.getAllUsers((err, result)=>{
        if(err){ 
            console.log('Database Error: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(result);
        res.end;
    });
});

// Get One User by id in Postgres matches to src_id in Mongo
// Returns User Object
router.get('/users/:id',(req, res) =>{
    user.getUsers(req.params.id, (err, result)=>{
        if(err){ 
            console.log('Database Error: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(result);
        res.end;
    });
});

// Update a User based on id (src_id in Mongo) -- Expects a Full User Obj
// Returns the updated user  as [PG user, MG user]-- 
// TO-DO Return User Obj
router.put('/users/:id', (req,res) =>{
    user.updateUsers(req.params.id, req.body, (err, result) =>{
        if(err){ 
            console.log('Database Error: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(result);
        res.end;
    });
});

// Create a new user, expects a User Obj (will auto-gen an id)
// Returns the New Id -- 
// TO-DO return the full User Object
router.post('/users', (req,res) =>{
    user.insertUsers(req.body, (err, result) =>{
        if(err){ 
            console.log('Database Error: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=201;
        res.json(result);
        res.end;
    });
});

// Marks is_deleted = true Expects an id
// Returns Success
router.delete('/users/:id',(req,res) =>{
    postgres.deleteUsers(req.params.id, (err, result) =>{
        if(err){ 
            console.log('Database Error: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(result);
        res.end;
    });
});

// Returns all Teams as [Team, Team, Team]
// Inculdes Parent Team
router.get('/teams', (req, res)=>{
    postgres.getAllTeams((err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Expects team id
// Returns Team Obj -- inculdes Parent team 
router.get('/teams/:id', (req, res)=>{
    postgres.getTeams(req.params.id, (err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Returns all locations [Location, Location...]
router.get('/locations', (req, res)=>{
    postgres.getAllLocations((err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Expects a Location id
// Returns a Location Obj
router.get('/locations/:id', (req, res)=>{
    postgres.getLocations(req.params.id, (err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Returns all companies [Company, Company ...]
router.get('/companies', (req, res)=>{
    postgres.getAllCompanies((err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Expects a Company Id
// Returns a Company Obj
router.get('/companies/:id', (req, res)=>{
    postgres.getCompanies(req.params.id, (err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Returns [Position, Position...]
router.get('/positions', (req, res)=>{
    postgres.getAllPositions((err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Expects a position Id
// Returns a Position Obj
router.get('/positions/:id', (req, res)=>{
    postgres.getPositions(req.params.id, (err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Returns all Roles [Role, Role...]
router.get('/user_role', (req, res)=>{
    postgres.getAllUserRoles((err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Expects a User ID -- (id or Src ID)
// Returns the Role Obj of that User
router.get('/user_role/:id', (req, res)=>{
    postgres.getUserRoles(req.params.id, (err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data.rows);
        res.end();
    });
});

// Returns a sorted list of skills based on how many times they are listed
// ex [{Java: 10 }, {Python: 6}, {NoSql: 3}, {x123x: 0}]        
router.get('/skills', (req, res)=>{
    mongo.getSkills((err, data) =>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data);
        res.end();
    });
});

// Expects a skill name (Java, Python ...)
// Returns a 
router.get('/skills/:name', (req, res)=>{
    user.getSkill(req.params.name, (err, data)=>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data);
        res.end();
    });
});

// Expects /userSearch?name=xyz where xyz is a string of letters
// Returns list of User Objects where xyz is part of their First, Last, Or Nicknames
router.get('/userSearch', (req, res) =>{
    user.getUserSearch(req.query.name, (err, data)=>{
        if(err){
            console.log('Database Err: ', err);
            res.statusCode=400;
            res.send(err);
            res.end(); 
            return;
        }
        res.statusCode=200;
        res.json(data);
        res.end();
    });
});

module.exports = router;

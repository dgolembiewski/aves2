"use strict";

const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const config = require('../cfg/config.js');

const project_member = require('../models/member');

// Required For File Upload
var formidable = require('formidable');
var Grid = require('gridfs-stream')
var fs = require('fs')
Grid.mongo = mongoose.mongo;

// const pg = require('pg');
// // connect to PostgreSQL db
// const dbPg = new pg.Client(config.POSTGRES_URL);
// dbPg.connect(function(err) {
//     if (err) {
//         console.log('\n\t PostgreSQL connection error!!!\n');
//     }
//     dbPg.query('SELECT NOW()', (err, res) => {
//         console.log(err, res);
//         dbPg.end();
//     });
// });

// Connect to MongoDB collection
const db = config.MONGODB_URL;
mongoose.Promise = global.Promise;
mongoose.connect(db, function (err) {
  if (err) {
    console.log('\n\tMongoDB connection error!!!\n');
  }
});

// MONGO ROUTES FOR MEMBERS
router.get('/members', (req, res) => {
  project_member.find({})
    .catch(err => console.log(err))
    .then(members => {
      // console.log(members);
      res.json(members);
    })
})

router.post('/members', (req, res) => {
  console.log(req.body)
  project_member.create(req.body)
    .catch(err => console.log(err))
    .then(member => {
      res.json(member)
    })
})

router.get('/members/:id', (req, res) => {
  project_member.findById(req.params.id)
    .catch(err => console.log(err))
    .then(member => res.json(member))
})

router.put('/members/:id', (req, res) => {
  project_member.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true })
    .catch(err => console.log(err))
    .then(member => {
      res.json(member)
    })
})

router.delete('/members/:id', (req, res) => {
  project_member.findOneAndRemove({ _id: req.params.id })
    .catch(err => console.log(err))
    .then(member => {
      res.json(member)
    })
})



// MONGO ROUTES FOR MEMBERS BY POSTGRES SOURCE ID
router.get('/members/src/:id', (req, res) => {
  project_member.findOne({ src_id: req.params.id })
    .catch(err => console.log(err))
    .then(member => {
      res.json(member)
    })
})

router.put('/members/src/:id', (req, res) => {
  project_member.findOneAndUpdate({ src_id: req.params.id }, req.body, { new: true })
    .catch(err => console.log("\n\t error \n"))
    .then(member => {
      res.json(member)
    })
})

//Route for file upload fron Angular-Front to Node to MongoDB
router.post('/upload', (req, res) => {
  var form = new formidable.IncomingForm();
  form.parse(req, function (error, fields, files) {
    var gfs = Grid(mongoose.connection.db, mongoose.mongo);
    var writestream = gfs.createWriteStream({
      filename: files.file.name,
      root: "project_members"
    });
    console.log('this is files', files.file.path)
    fs.createReadStream(files.file.path).pipe(writestream);

    writestream.on('close', function (file) {
      console.log(req);
      req.params.resume = files
    })
  })
})

router.get('/file/:filename', (req, res) => {
  var gfs = Grid(mongoose.connection.db, mongoose.mongo);
  gfs.collection('project_members');
  gfs.files.find({ filename: req.params.filename }).toArray((err, files) => {
    if (!files || files.elength === 0) {
      return res.status(404).json({
        responseMessage: "Error Cannot be Found"
      });
    }
    var readstream = gfs.createReadStream({
      filename: files[0].filename,
      root: "project_members"
    });
    res.set('Content-Type', files[0].contentType)
    return readstream.pipe(res);
  })
  console.log('this is res', res)
})

module.exports = router;

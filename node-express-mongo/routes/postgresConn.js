
'use strict';

const pg = require('pg');
const config = require('../cfg/config.js');
const project_member = require('../models/member');
const scripts = require('../scripts');
const user = require('../models/postgresUser');

const { Pool } = require('pg');

// Uses Scripts File for most sql querys
// Uses Pool to allow more than 1 connection to support multiple users
class PostgresConn {
    constructor(){
        this.pool = new Pool({connectionString:config.POSTGRES_URL, max:config.MAX_CON});
        this.pool.query("SET application_name = Node_PG_Backend", (err, res)=>{
            if(err){
                console.log("DB Connection Error");
            }
        })
    };
// router.get('/users',(req, res) =>{
    getAllUsers(cb) {
        this.pool.query(scripts.getAllUsers, (err, data) =>{
            if (err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null,data);
        });
    };

    getUsers(id, cb) {
        this.pool.query(scripts.getUsers, [id], (err, data) =>{
            if (err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null,data);
        });
    }

    updateUsers(id, body, cb){ // we have to build our query along with our data here
        var sqlList = ['UPDATE aves.user SET '];
        var update = [];
        var data = []
        var i = 1;
        Object.entries(body).forEach((element) => {
            if (user.hasOwnProperty(element[0])){ //Part of user table (not team, location.. etc)
                update.push(user[element[0]] +'= $' + (i++) );
                data.push(element[1])
            }
        });
        sqlList.push(update.join(', '));
        sqlList.push('WHERE id = $' +i +' and is_deleted = false');
        data.push(id);
        sqlList.push('RETURNING *');
        var sql = sqlList.join(' ');
        this.pool.query(sql, data, (err,data_r)=>{
            if(err){
                return cb(err, null);
            }
            return cb(null, data_r);
        });
    }

    insertUsers(body, cb){
        var sqlList = ['INSERT INTO aves.user ('];
        var columnList = [];
        var valList = []
        var data = []
        var i = 1;
        Object.entries(body).forEach((element) => {
            if (user.hasOwnProperty(element[0]) && element[0] != 'id'){ //Part of user table (not team, location.. etc)
                columnList.push(user[element[0]]);
                valList.push('$'+i++);
                data.push(element[1]);
            }
        });
        sqlList.push(columnList.join(', '));
        sqlList.push(')')
        sqlList.push('VALUES (');
        sqlList.push(valList.join(', '));
        sqlList.push(')');
        sqlList.push('RETURNING *');
        var sql = sqlList.join(' ');
        this.pool.query(sql, data, (err,data_r)=>{
            if(err){
                return cb(err, null);
            }
            return cb(null, data_r);
        });
    }

    deleteUsers(id, cb){
        this.pool.query(scripts.deleteUser, [id], (err,data_r)=>{
            if(err){
                return cb(err, null);
            }
            return cb(null, data_r);
        });
    }


    getAllTeams(cb) {
        this.pool.query(scripts.getAllTeams, (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getTeams(id, cb) {
        this.pool.query(scripts.getTeams, [id], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getAllLocations(cb) {
        this.pool.query(scripts.getAllLocations, (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getLocations(id, cb) {
        this.pool.query(scripts.getLocations, [id], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getAllCompanies(cb) {
        this.pool.query(scripts.getAllCompanies, (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getCompanies(id, cb) {
        this.pool.query(scripts.getCompanies, [id], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getAllPositions(cb) {
        this.pool.query(scripts.getAllPositions, (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getPositions(id, cb) {
        this.pool.query(scripts.getPositions, [id], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getAllUserRoles(cb) {
        this.pool.query(scripts.getAllUserRoles, (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getUserRoles(id, cb) {
        this.pool.query(scripts.getUserRoles, [id], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getAllUserActivations(cb) {
        this.pool.query(scripts.getAllUserActivations, (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }


    getUserActivations(id, cb) {
        this.pool.query(scripts.getUserActivation, [id], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    //User Util Functions
    getTeamNames(cb){
        this.pool.query(scripts.getAllTeamsUser, (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getSingleTeamName(id, cb){
        this.pool.query(scripts.getTeamUser, [id], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getUsersSearch(name, cb){
        this.pool.query(scripts.getUserSearch, ['%'+name+'%'],(err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

    getUsersById(src, cb){
        this.pool.query(scripts.getUsersById, [src], (err, data)=>{
            if(err){
                console.log('Query Error: ', err);
                return cb(err, null);
            }
            cb(null, data);
        });
    }

}

module.exports = PostgresConn;





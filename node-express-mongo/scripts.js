'use strict';
let scripts ={

    //Get calls
    getAllUsers: 'SELECT * FROM aves.user where is_deleted = false',
    getAllTeams: 'SELECT * FROM aves.team',
    getAllLocations: 'SELECT * FROM aves.location',
    getAllCompanies: 'SELECT * FROM aves.company',
    getAllPositions: 'SELECT * FROM aves.position',
    getAllUserRoles: 'SELECT * FROM aves.user_role',
    getAllUserActivations: 'SELECT * FROM aves.user_activation',
    getUsers: 'SELECT * FROM aves.user where id = $1 and is_deleted = false',
    getTeams: 'SELECT * FROM aves.team where id = $1',
    getLocations: 'SELECT * FROM aves.location where id = $1',
    getCompanies: 'SELECT * FROM aves.company where id = $1',
    getPositions: 'SELECT * FROM aves.position where id = $1',
    getUserRoles: 'SELECT * FROM aves.user_role where user_id = $1',
    getUserActivation: 'SELECT * FROM aves.user_activation where user_id = $1',

    //Delete Calls
    deleteUser: "UPDATE aves.user SET is_deleted = true where id = $1",

    //Util 
    getAllTeamsUser: 'SELECT tm.name as "Subteam", tm.id as "Subteam_Id", \
    pt.name as "Parent_Team", pt.id as "Parent_Team_Id"\
    from aves.team as tm \
   inner join aves.team as pt on \
   tm.parent_team_id = pt.id',
    getTeamUser: 'SELECT tm.name as "Subteam", tm.id as "Subteam_Id", \
    pt.name as "Parent_Team", pt.id as "Parent_Team_Id"\
     from aves.team as tm \
    inner join aves.team as pt on \
    tm.parent_team_id = pt.id where tm.id = $1',
    getUserSearch: "SELECT * FROM aves.user where (lower(first_name) like lower( $1 ) OR \
    lower(last_name) like lower( $1 ) OR lower(nick_name) like lower( $1 )) and is_deleted = false",
    getUsersById: "SELECT * FROM aves.user where is_deleted = false and id = ANY($1::uuid[])"
}



module.exports = Object.freeze(scripts);

#!/bin/bash
# port to start node server on
PORT=3000
export PORT

# mongo db connection string
# MONGODB_URL="mongodb://khiem:falcon@ds139964.mlab.com:39964/falconclients"
MONGODB_URL="mongodb://avesuser:avesuser@ec2-54-210-232-74.compute-1.amazonaws.com:27017/avesdb"
export MONGODB_URL

# postgres db connection string
# POSTGRES_URL="postgres://avesadmin:avesadmin@aves-master.c0azff0quula.us-east-1.rds.amazonaws.com:5432/avesmaster"
POSTGRES_URL="postgres://avesadmin:avesadmin@localhost:5432/aves_leef"
export POSTGRES_URL


MAX_CON=5
export MAX_CON

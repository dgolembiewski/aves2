"use strict";

!(function() {
    var key = ''
    ;

    const cfg = {
        PORT: process.env.PORT,
        MONGODB_URL: process.env.MONGODB_URL,
        POSTGRES_URL: process.env.POSTGRES_URL,
        MAX_CON : process.env.MAX_CON
    };

    console.log('*** running config start ***');
    for (key in cfg) {
        console.log(key + ': ' + cfg[key]);
    }
    console.log('*** running config end ***');

    module.exports = cfg;
})();
